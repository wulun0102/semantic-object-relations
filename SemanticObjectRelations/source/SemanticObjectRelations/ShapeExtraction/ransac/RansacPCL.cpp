#include "RansacPCL.h"

#include <SemanticObjectRelations/Hooks/Log.h>

#include <pcl/common/io.h>

namespace semrel
{

RansacPCL::RansacPCL()
{
}

RansacPCL::RansacPCL(int maxIterations, float distanceThreshold) :
    RansacBase(maxIterations, distanceThreshold)
{
}

void RansacPCL::setPointCloud(const pcl::PointCloud<PointL>& pointCloud)
{
    RansacBase::setPointCloud(pointCloud);
    
    // convert and copy point cloud to own point cloud
    this->pointCloud.reset(new pcl::PointCloud<PointR>());
    pcl::copyPointCloud<PointL, PointR>(pointCloud, *(this->pointCloud));
}

RansacResult RansacPCL::fitShape(const pcl::PointCloud<semrel::PointL>& pointCloud)
{
    SR_LOG_VERBOSE << "== STARTING PCL " << shapeName() << " RANSAC ==";
    setPointCloud(pointCloud);

    pcl::SampleConsensusModel<PointR>::Ptr model = getSampleConsensusModel();

    pcl::RandomSampleConsensus<PointR> ransac(model);
    ransac.setDistanceThreshold(distanceThreshold);
    ransac.setMaxIterations(maxIterations);

    SR_LOG_DEBUG << "Computing model ...";
    bool found = ransac.computeModel();

    if (!found)
    {
        SR_LOG_VERBOSE << "== PCL " << shapeName() << " RANSAC FAILED (No good model found) ==";
        return RansacResult();
    }
    
    SR_LOG_DEBUG << "Getting inliers ...";
    
    std::vector<int> inlierIndices;
    ransac.getInliers(inlierIndices);

    if (inlierIndices.empty())
    {
        SR_LOG_VERBOSE << "== PCL " << shapeName() << " RANSAC FAILED (No inliers) ==";
        return RansacResult();
    }
    
    
    SR_LOG_DEBUG << "Building shape ...";
    Eigen::VectorXf coefficients(model->getModelSize());
    ransac.getModelCoefficients(coefficients);

    std::unique_ptr<Shape> shape = makeShape(coefficients, inlierIndices);

    
    SR_LOG_DEBUG << "Evaluating model ...";
    Evaluation eval = evaluate(*shape, 100 * distanceThreshold * distanceThreshold);
    

    SR_LOG_VERBOSE << "== PCL " << shapeName() << " RANSAC COMPLETE ("
                   << "error: " << eval.error << ") ==";
    
    return RansacResult(std::move(shape), eval.error, eval.coverage);
}

}
