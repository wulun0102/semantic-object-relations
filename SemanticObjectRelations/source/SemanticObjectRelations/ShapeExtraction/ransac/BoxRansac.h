#pragma once

#include <SemanticObjectRelations/Shapes/Box.h>

#include "RansacBase.h"
#include "BoxFitter.h"

#include <optional>
#include <random>


namespace semrel
{

/**
 * @brief RANSAC for boxes.
 *
 * @param Minimal inlier rate: (default: 0.75)
 *      A candidate model is discarded if the proportion of inliers to the
 *      number of points in the point cloud is less than this value.
 * @param Outlier rate: (default: 0.01)
 *      In addition to outliers being declected for error computation,
 *      they are also neglected when estimating the extents of the box
 *      along its axes.
 * @param @see RansacBase
 */
class BoxRansac : public RansacBase
{
    
public:

    /// Constructor with parameters.
    BoxRansac(int maxIterations = 100,
              float distanceThreshold = 5.0,
              float minInlierRate = 0.75,
              float outlierRate = 0.01f);


    /// @see RansacBase
    virtual RansacResult fitShape(const pcl::PointCloud<PointL>& pointCloud) override;

    void setUseRandomSeed(bool useRandomSeed) override;

    /// Set the minimal inlier rate.
    void setMinInlierRate(float minInlierRate);

    
protected:
    
    /// Returns "Box".
    virtual std::string shapeName() const override;

    /// Returns the box's maximal extent as size measure.
    virtual float getSizeMeasure(const Shape& shape) const override;

    
private:
    
    /// Select a minimal sampling set from points with random, unique points.
    std::vector<Eigen::Vector3f> selectMSS();

    /**
     * @brief Attempts to fit one model from the given MSS.
     * @param iteration the iteration (for logging)
     * @param quickFail (out)
     *      Is set to true if this iteration failed quickly. That is, if
     *      the MSS was ill-suited (e.g. coplanar) or the determined
     *      orientation produced too few inliers. These cases are detected
     *      rather quickly, and thus may not be fully counted as iteration.
     * @return a found model, or nullptr if no model was found in this iteration
     */
    std::optional<Box> runIteration(std::vector<Eigen::Vector3f> mss,
                                    int iteration, bool& quickFail);


    // HELPERS

    /// The box fitter.
    BoxFitter boxFitter;


    // RESULTS

    /// Number of iterations where a good model was found.
    std::size_t goodModels = 0;
    /// Number of iterations where the model had too few inliers.
    std::size_t trivialFails = 0;


    // PARAMETERS

    /// The minimal inlier rate for a good model.
    float minInlierRate;

    std::default_random_engine randomEngine;
    
};

}
