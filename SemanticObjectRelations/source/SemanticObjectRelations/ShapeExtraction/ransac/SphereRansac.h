#pragma once

#include <SemanticObjectRelations/Shapes/Sphere.h>

#include "RansacPCL.h"


namespace semrel
{

/**
 * @brief RANSAC for spheres based on PCL sample consensus.
 *
 * @param @see RansacBase
 */
class SphereRansac : public RansacPCL
{
    
public:

    /// Constructor for default parameters.
    SphereRansac();

    void setUseRandomSeed(bool useRandomSeed) override;

    
protected:

    /// @see RansacPCL::getSampleConsensusModel()
    virtual pcl::SampleConsensusModel<PointR>::Ptr getSampleConsensusModel() override;

    /// @see RansacPCL::makeShape()
    virtual std::unique_ptr<Shape> makeShape(const Eigen::VectorXf& coefficients,
                                             const std::vector<int>& inlierIndices) override;

    /// Returns "Sphere".
    virtual std::string shapeName() const override;
    /// Returns the sphere's radius as size measure.
    virtual float getSizeMeasure(const Shape& shape) const override;

private:
    bool useRandomSeed = false;
    
};

}
