#pragma once

#include <SemanticObjectRelations/Shapes/Box.h>


namespace semrel
{

/**
     * @brief Offers subroutines for RANSAC box fitting.
     */
class BoxFitter
{

public:

    /// Constructor.
    BoxFitter(float outlierRate = 0.0);


    /**
     * @brief Compute box axes from the given MSS.
     * @return box axes hypothesis
     *  or zero matrix if point 4 or 5 is coplanar with points 1, 2 and 3
     * @throws std::invalid_argument If mss does not contain at least getMssSize() points.
     */
    Eigen::Matrix3f fitBoxAxesToMSS(const std::vector<Eigen::Vector3f>& mss);

    /**
     * @brief Compute the three visible planes from the given axes and the
     * point cloud.
     * @param axes as computed by fitBoxAxesToMSS()
     */
    std::vector<Hyperplane3f> computeVisiblePlanes(
            const Eigen::Matrix3f& axes, const std::vector<Eigen::Vector3f>& points);

    /**
     * @brief Compute the final box hypothesis from the visible planes.
     * The given vectors may be reordered.
     *
     * @param visiblePlanes The three visible planes as computed by computeVisiblePlanes().
     * @param inliers The inliers of each plane (size 3).
     * @return the box hypothesis
     */
    Box computeBox(std::vector<Hyperplane3f>& visiblePlanes,
                   std::vector<Eigen::Vector3f> (& planeInliers)[3]);


    /// Returns the MSS size.
    int getMssSize() const;

    
private:

    /// The number of points in the minimal sample set.
    static const int MSS_SIZE = 5;

    void sortByNumberOfInliers(std::vector<Hyperplane3f>& visiblePlanes,
                               std::vector<Eigen::Vector3f> (& planeInliers)[3]);

    std::pair<float, Eigen::Vector3f> computeDimAndNormal(
            float minT, float maxT,
            const ParametrizedLine3f& line,
            const ParametrizedLine3f& line12,
            const Hyperplane3f& plane);


    /// The outlier rate.
    float outlierRate = 0;


};

}
