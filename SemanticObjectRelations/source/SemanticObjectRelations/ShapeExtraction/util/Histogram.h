#pragma once

#include <vector>
#include <ostream>

namespace semrel
{

/**
 * @brief Histogram for one-dimensional float data.
 */
class Histogram
{
public:

    /// No initialization constructor.
    Histogram();

    /// Construct from the given data.
    Histogram(const std::vector<float>& data, std::size_t numberOfBins = 128);

    /// Initialize from the given data.
    void initialize(const std::vector<float>& data, std::size_t numberOfBins = 128);


    ///  Transfer the given value to its corresponding bin index.
    std::size_t valueToIndex(float value) const;

    /// Returns the value at the center of the bin with the given index.
    float indexToValue(std::size_t index) const;

    /// Inserts the given value into the histogram.
    void insert(float value);

    /// Get the bins.
    const std::vector<std::size_t>& getBins() const;

    /// Get the number of bins.
    std::size_t getNumberOfBins() const;

    /// Get the minimum mapped value.
    float getMinMappedValue() const;
    /// Get the maximum mapped value.
    float getMaxMappedValue() const;

    /// Returns the index of the bin containing the fewest data points.
    std::size_t getMinBinIndex() const;
    /// Returns the corresponding value of the bin containing the fewest data points.
    float getMinBinValue() const;

    /// Returns the index of the bin containing the most data points.
    std::size_t getMaxBinIndex() const;
    /// Returns the corresponding value of the bin containing the most data points.
    float getMaxBinValue() const;


    /**
         * @brief Applies a median filter to the hisogram bins.
         *
         * The size specifies the number of neighours (in each direction) considered.
         * That is, if k = size, 2k+1 values are considered for each point (k neighbours
         * in each direction).
         *
         * @param size the size of the neighbourhood (in each direction)
         */
    void applyMedianFilter(std::size_t size = 2);

    /// Returns the value of the biggest peak, and removes it by emptying
    /// its correspondig bins.
    float popPeak();


    /// Streams a CVS-like description of the histogram containing the bin data.
    friend std::ostream& operator<<(std::ostream& os, const Histogram& histo);


private:

    /// The minimum mapped value.
    float min;
    /// The maximum mapped value.
    float max;

    /// The bins.
    std::vector<std::size_t> bins;

};

}
