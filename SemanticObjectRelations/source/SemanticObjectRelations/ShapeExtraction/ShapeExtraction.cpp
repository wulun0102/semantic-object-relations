#include "ShapeExtraction.h"

#include <future>

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/Visualizer.h>

namespace semrel
{

ShapeExtraction::ShapeExtraction(float modelErrorThreshold) :
    maxModelError(modelErrorThreshold)
{
}

ShapeExtractionResult ShapeExtraction::extract(
        const pcl::PointCloud<pcl::PointXYZRGBL>& labeledPointCloud)
{
    PointCloudSegments inputSegments(labeledPointCloud);
    
    std::vector<uint32_t> labels = inputSegments.getLabels();
    
    SR_LOG_INFO << "=== STARTING SHAPE EXTRACTION ===";
    SR_LOG_INFO << "Point cloud: " << inputSegments.numPoints() << " points in "
                << inputSegments.numSegments() << " segments.";
    
    
    // start one thread per segment
    
    using RansacTriple = std::tuple<BoxRansac, CylinderRansac, SphereRansac>;
    using RansacResultTriple = std::tuple<RansacResult, RansacResult, RansacResult>;
    
    std::map<uint32_t, std::future<RansacResultTriple>> segmentFutures;
    std::vector<RansacTriple> segmentRansacs;
    
    for (uint32_t label : labels)
    {
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr segment = inputSegments.getSegment(label);
        
        
        std::launch policy;
        if (enabledConcurrency)
        {
            policy = std::launch::async;
            SR_LOG_VERBOSE << "Starting extraction in segment " << label;
        }
        else
        {
            policy = std::launch::deferred;
            SR_LOG_VERBOSE << "Queuing extraction in segment " << label;
        }
        
        segmentFutures[label] = std::async(policy, [this, segment]()
        {
            // create locals from prototypes
            BoxRansac boxRansac = protoBoxRansac;
            CylinderRansac cylinderRansac = protoCylinderRansac;
            SphereRansac sphereRansac = protoSphereRansac;
            
            return std::make_tuple(boxRansac.fitShape(*segment),
                                   cylinderRansac.fitShape(*segment),
                                   sphereRansac.fitShape(*segment));
        });
    }
    
    SR_LOG_INFO << "Extraction started for all segments";
    
    
    // wait for completion and collect results
    
    ShapeExtractionResult result;
    
    for (uint32_t label : labels)
    {
        // wait for completion
        SR_LOG_VERBOSE << "Waiting for segment " << label << " ...";
        
        RansacResultTriple ransacResults = segmentFutures[label].get();
        
        SR_LOG_VERBOSE << "Segment " << label << " finished.";
        
        
        // collect results
        
        // get shape of smallest error
        RansacResult finalResult;
        finalResult.info.error = std::numeric_limits<float>::max();
        
        auto keepBestResult = [&](RansacResult & result)
        {
            if (result.shape && result.info.error < finalResult.info.error)
            {
                finalResult = std::move(result);
            }
        };
        
        if (enabledBoxExtraction) keepBestResult(std::get<0>(ransacResults));
        if (enabledCylinderExtraction) keepBestResult(std::get<1>(ransacResults));
        if (enabledSphereExtraction) keepBestResult(std::get<2>(ransacResults));
        
        if (finalResult.info.error >= maxModelError)
        {
            // "no shape found"
            finalResult.shape = nullptr;
        }
        
        SR_LOG_VERBOSE << "Segment " << label << ": error: " << finalResult.info.error << ", shape: "
                       << (finalResult.shape ? finalResult.shape->str() : "--");
        
        if (finalResult.shape)
        {
            finalResult.shape->setID(ShapeID(label));
            result.objects.push_back(std::move(finalResult.shape));
        }
        
        result.extractionInfo.emplace(label, finalResult.info);
    }
    
    SR_LOG_INFO << "=== SHAPE EXTRACTION FINISHED ===";
    
    SR_LOG_VERBOSE << "Extracted shapes:";
    for (const auto& segmentResults : result.extractionInfo)
    {
        uint32_t label = segmentResults.first;
        const RansacResultInfo& info = segmentResults.second;
        SR_LOG_VERBOSE << "Segment " << label << ": \n"
                       << "| error: " << info.error << "\n"
                       << "| cover: " << info.coverage << "\n";
    }
    
    
    static Visualizer visu("ShapeExtraction");
    visu.drawShapeList(VisuLevel::RESULT, result.objects, 0.5f, true);
    
    return result;
}

void ShapeExtraction::useRandomSeed(bool useRandomSeed)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setUseRandomSeed(useRandomSeed);
    }
}

void ShapeExtraction::setMaxModelError(float maxError)
{
    this->maxModelError = maxError;
}

void ShapeExtraction::setMaxIterations(int iterations)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setMaxIterations(iterations);
    }
}

void ShapeExtraction::setDistanceThreshold(float distanceThreshold)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setDistanceThreshold(distanceThreshold);
    }
}

void ShapeExtraction::setDistanceThresholdBox(float distanceThreshold)
{
    protoBoxRansac.setDistanceThreshold(distanceThreshold);
}

void ShapeExtraction::setDistanceThresholdPCL(float distanceThreshold)
{
    protoCylinderRansac.setDistanceThreshold(distanceThreshold);
    protoSphereRansac.setDistanceThreshold(distanceThreshold);
}

void ShapeExtraction::setOutlierRate(float outlierRate)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setOutlierRate(outlierRate);
    }
}

void ShapeExtraction::setMinInlierRate(float minInlierRate)
{
    protoBoxRansac.setMinInlierRate(minInlierRate);
}

void ShapeExtraction::setSizePenalty(float factor, float exponent)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setSizePenalty(factor, exponent);
    }
}

void ShapeExtraction::setSizePenaltyFactor(float factor)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setSizePenaltyFactor(factor);
    }
}

void ShapeExtraction::setSizePenaltyExponent(float exponent)
{
    for (RansacBase& ransac : allRansacs)
    {
        ransac.setSizePenaltyExponent(exponent);
    }
}

void ShapeExtraction::setBoxExtractionEnabled(bool enabled)
{
    enabledBoxExtraction = enabled;
}

void ShapeExtraction::setCylinderExtractionEnabled(bool enabled)
{
    enabledCylinderExtraction = enabled;
}

void ShapeExtraction::setSphereExtractionEnabled(bool enabled)
{
    enabledSphereExtraction = enabled;
}

void ShapeExtraction::setConcurrencyEnabled(bool enabled)
{
    enabledConcurrency = enabled;
}

}
