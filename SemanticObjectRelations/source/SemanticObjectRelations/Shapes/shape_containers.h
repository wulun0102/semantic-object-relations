#pragma once

#include <map>
#include <memory>
#include <vector>

#include "exceptions.h"
#include "Shape.h"


namespace semrel
{
    /**
     * @brief An owning list of Shapes.
     *
     * A container class for Shape instances designed to be the single unique
     * place where `Shape` instances are kept.
     */
    using ShapeList = std::vector<std::unique_ptr<Shape>>;


    /**
     * @brief A non-owning map from shape ID to (raw) shape pointers.
     *
     * At all times, the following invariant must hold for all keys `k` in a
     * `ShapeMap m`:
     * @code
     * k == m.at(k)->getID()
     * @endcode
     */
    using ShapeMap = std::map<ShapeID, Shape*>;


    /**
     * @brief Construct a `ShapeMap` from the given `ShapeList`.
     *
     * If an ID occurs in more than one shape in `shapes`, the returned map will
     * contain the last occuring shape with that ID and will have less elements
     * than `shapes`.
     *
     * @throw `error::error::ObjectPointerIsNull` If an element of `shapes` is null.
     */
    ShapeMap toShapeMap(const ShapeList& shapes);

    /// @see toShapeMap()
    ShapeMap toShapeMap(const std::vector<Shape*>& shapes);


    /// Print a description of `shapeList`'s contents to `os`.
    std::ostream& operator <<(std::ostream& os, const ShapeList& shapeList);

    /// Print a description of `shapeMap`'s contents to `os`.
    std::ostream& operator <<(std::ostream& os, const ShapeMap& shapeMap);

}

