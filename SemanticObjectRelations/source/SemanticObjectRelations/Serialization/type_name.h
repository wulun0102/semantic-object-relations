#pragma once

#include <string>
#include <typeinfo>


namespace semrel
{
    class Shape;
}

namespace semrel::serial
{

    /**
     * @brief Demangles the given type name.
     * @see `boost::core::demangle()`
     */
    std::string demangle(const char* typeName);


    /// Return the demangled type name in `typeInfo`.
    std::string getTypeName(const std::type_info& typeInfo);

    /// Return the demangled (static) type name of `t`.
    template <typename T>
    std::string getTypeName()
    {
        return getTypeName(typeid (T));
    }

    /// Return the demangled (dynamic) type name of `t`.
    template <typename T>
    std::string getTypeName(const T& t)
    {
        return getTypeName(typeid (t));
    }

}

