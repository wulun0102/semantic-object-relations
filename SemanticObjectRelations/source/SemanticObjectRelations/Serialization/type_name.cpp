#include "type_name.h"

#include <boost/core/demangle.hpp>


namespace semrel
{

    std::string serial::demangle(const char* typeName)
    {
        return boost::core::demangle(typeName);
    }

    std::string serial::getTypeName(const std::type_info& typeInfo)
    {
        return demangle(typeInfo.name());
    }

}
