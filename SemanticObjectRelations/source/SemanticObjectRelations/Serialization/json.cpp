#include "json.h"


namespace semrel
{

std::string serial::getTypeName(const nlohmann::json& j, const std::string& key)
{
    try
    {
        return j.at(key);
    }
    catch (const nlohmann::detail::out_of_range&)
    {
        throw error::NoTypeNameEntryInJsonObject(key, j);
    }
    catch (const nlohmann::detail::type_error&)
    {
        throw error::NoTypeNameEntryInJsonObject(key, j);
    }
}

void serial::setTypeName(nlohmann::json& j, const std::string& key, const std::string& typeName)
{
    if (j.count(key) > 0)
    {
        throw error::TypeNameEntryAlreadyInJsonObject(key, typeName, j);
    }
    j[key] = typeName;
}

}
