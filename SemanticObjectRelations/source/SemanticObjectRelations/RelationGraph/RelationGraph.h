#pragma once

#include <cstdio>

#include <boost/graph/adjacency_list.hpp>

#include <SemanticObjectRelations/Serialization/type_name.h>

#include <SemanticObjectRelations/Shapes/exceptions.h>
#include <SemanticObjectRelations/Shapes/Shape.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>

#include "graphviz.h"
#include "VertexEdge.h"


namespace semrel
{

/// No attributes.
struct NoAttrib { };

/**
 * @brief A vertex representing an object (Shape).
 */
struct ShapeVertex
{
    /// The object ID.
    ShapeID objectID { -1 };
};


/// Base class for RelationGraph (bidirectional with std::vector as
/// vertex container std::list as edge container).
template <class VertexAttrib, class EdgeAttrib, class GraphAttrib>
using RelationGraphBoost = boost::adjacency_list<boost::listS, boost::vecS, boost::bidirectionalS,
VertexAttrib, EdgeAttrib, GraphAttrib>;

// boost::listS as vertex container does not work with boost::write_graphviz and
// adds some other complications (may be caused by a bug in our boost version)


/**
 * @brief Representation of a binary relation between objects as a directed graph.
 *
 * This class is based on `boost::adjacency_list`, with wrapper methods
 * for easier access and iteration. Three template parameters can be
 * specified for custom vertex, edge and graph attributes.
 *
 * Vertex attributes must derive from `ShapeVertex` (which is also the
 * default). Therefore, they contain at least one object (more specifically,
 * a pointer to it). Edge and graph attributes can be chosen freely (with no
 * attributes being the default).
 *
 * A `RelationGraph` can be constructed empty or from a `ShapeList`.
 *
 * Vertex and edge can be accessed via vertex descriptors (edges), object
 * IDs or objects. Object equality is defined in terms of their IDs:
 * If two objects have the same ID, they are considered equal (types are
 * not checked).
 *
 * @param VertexAttrib Vertex attributes. Must derive from `ShapeVertex` (default: `ShapeVertex`)
 * @param EdgeAttrib Edge attributes (default: no attributes)
 * @param GraphAttrib Edge attributes (default: no attributes)
 */
template <class VertexAttribT = ShapeVertex,
          class EdgeAttribT = NoAttrib,
          class GraphAttribT = NoAttrib>
class RelationGraph : public RelationGraphBoost<VertexAttribT, EdgeAttribT, GraphAttribT>
{
public:

    // TYPE DEFINITIONS

    using VertexAttrib = VertexAttribT;  /// The vertex attributes.
    using EdgeAttrib = EdgeAttribT;      /// The edge attributes.
    using GraphAttrib = GraphAttribT;    /// The graph attributes.

    using Boost = RelationGraphBoost<VertexAttrib, EdgeAttrib, GraphAttrib>; ///< Native boost type.
    using Traits = boost::graph_traits<RelationGraph>;         ///< Graph traits.

    using ConstVertex = ConstVertexT<RelationGraph>; ///< Vertex (const).
    using Vertex = VertexT<RelationGraph>;           ///< Vertex (non-const).
    using ConstEdge = ConstEdgeT<RelationGraph>;     ///< Edge (non-const).
    using Edge = EdgeT<RelationGraph>;               ///< Edge (non-const).

    /// Native boost vertex descriptor.
    using VertexDescriptor = typename Traits::vertex_descriptor;
    /// Native boost edge descriptor.
    using EdgeDescriptor = typename Traits::edge_descriptor;

    /// Mutable edge range.
    using EdgeRange = detail::Range<typename Traits::edge_iterator, detail::DescriptorTo<Edge>>;
    /// Constant edge range.
    using ConstEdgeRange = detail::Range<typename Traits::edge_iterator, detail::DescriptorTo<ConstEdge>>;
    /// Mutable vertex range.
    using VertRange = detail::Range<typename Traits::vertex_iterator, detail::DescriptorTo<Vertex>>;
    /// Constant vertex range.
    using ConstVertRange = detail::Range<typename Traits::vertex_iterator, detail::DescriptorTo<ConstVertex>>;


public:

    // CONSTRUCTORS AND METHODS

    /// Constructs an empty graph, optionally with given graph attributes.
    RelationGraph(const GraphAttrib& attribs = {});

    /// Constructs a graph with one vertex for each given object ID (with optional graph attributes).
    explicit RelationGraph(const std::vector<ShapeID>& objectIDs, const GraphAttrib& attribs = {});

    /// Constructs a graph with one vertex for each given object (with optional graph attributes).
    explicit RelationGraph(const ShapeList& objects, const GraphAttrib& attribs = {});

    /// Constructs a graph with one vertex for each given object (with optional graph attributes).
    explicit RelationGraph(const ShapeMap& objects, const GraphAttrib& attribs = {});

    /**
     * @brief Construct from raw boost graph.
     *
     * It may be necessary to reimplement this constructor in your derived
     * RelationGraph class, as some boost graph functions need to take
     * raw boost objects (deriving deletes some constructors of the raw
     * boost graph type). In that case, use asBoost() to get this graph
     * as raw boost object.
     */
    explicit RelationGraph(const RelationGraphBoost<VertexAttrib, EdgeAttrib, GraphAttrib>& boost)
        : Boost(boost)
    { }


    /// Construction from number of elements without objects is not permitted.
    RelationGraph(std::size_t num) = delete;


    /// Virtual destructor.
    virtual ~RelationGraph() {}


public:

    /// Get this graph as raw boost graph.
    Boost asBoost() const { return *this; }


    // GRAPH ATTRIBUTES

    /// Get the graph attributes.
    GraphAttrib& attrib() { return Boost::operator[](boost::graph_bundle); }
    const GraphAttrib& attrib() const { return const_cast<RelationGraph*>(this)->attrib(); }


    // VERTEX ACCESS

    /// Indicate whether there is a vertex with the given index.
    bool hasVertex(std::size_t index) const { return index < numVertices(); }
    /// Indicate whether there is a vertex with the given object ID.
    bool hasVertex(ShapeID id) const;
    /// Indicate whether there is a vertex with the given object.
    bool hasVertex(const Shape& obj) const { return hasVertex(obj.getID()); }

    /**
     * @brief Get the vertex descriptor of the vertex with the given index.
     * @throw error::NoVertexAtIndex index >= numVertices()
     */
    Vertex vertex(std::size_t index);
    ConstVertex vertex(std::size_t index) const { return const_cast<RelationGraph*>(this)->vertex(index); }
    /**
     * @brief Search for a vertex whose object has the given ID and get the vertex.
     * @throw NoVertexWithID if no vertex with the given ID was found.
     */
    Vertex vertex(ShapeID id);
    ConstVertex vertex(ShapeID id) const { return const_cast<RelationGraph*>(this)->vertex(id); }
    /**
     * @brief Search for a vertex whose object has the same ID as the given object and Get the vertex.
     * @throw NoVertexWithID if no vertex with the given object's ID was found.
     */
    Vertex vertex(const Shape& object) { return vertex(object.getID()); }
    ConstVertex vertex(const Shape& object) const { return const_cast<RelationGraph*>(this)->vertex(object); }


    // EDGE ACCESS

    /// Indicate whether the graph contains an edge from u to v.
    bool hasEdge(VertexDescriptor u, VertexDescriptor v) const { return boost::edge(u, v, *this).second; }
    /// Indicate whether the graph contains an edge from u to v.
    bool hasEdge(ConstVertex u, ConstVertex v) const { return hasEdge(u.descriptor(), v.descriptor()); }
    /**
     * @brief Indicate whether the graph contains an edge from vertex(id1) to vertex(id2).
     * @throw NoVertexWithID if there is no vertex with id1 or id2.
     */
    bool hasEdge(ShapeID id1, ShapeID id2) const { return hasEdge(vertex(id1), vertex(id2)); }
    /**
     * @brief Indicate whether the graph contains an edge from vertex(obj1) to vertex(obj2).
     * @throw NoVertexWithID if there is no vertex with obj1 or obj2.
     */
    bool hasEdge(const Shape& obj1, const Shape& obj2) const { return hasEdge(vertex(obj1), vertex(obj2)); }

    /**
     * @brief Get the descriptor of the edge from u to v, if that edge exists.
     * @throw error::NoEdgeBetween if there is no edge from u to v.
     */
    EdgeDescriptor edgeDescriptor(VertexDescriptor u, VertexDescriptor v) const;

    /// Get the edge identified by the edge descriptor.
    ConstEdge edge(EdgeDescriptor e) const { return ConstEdge(this, e); }
    Edge edge(EdgeDescriptor e) { return Edge(this, e); }

    /**
     * @brief Get the edge from u to v, if that edge exists.
     * @throw error::NoEdgeBetween if there is no edge from u to v.
     */
    ConstEdge edge(ConstVertex u, ConstVertex v) const { return ConstEdge(this, edgeDescriptor(u.descriptor(), v.descriptor())); }
    Edge edge(ConstVertex u, ConstVertex v) { return Edge(this, edgeDescriptor(u.descriptor(), v.descriptor())); }




    // FURTHER EDGE INFORMATION

    /// Indicate whether there is a path from u to v.
    bool hasPath(VertexDescriptor u, VertexDescriptor v) const;
    /// Indicate whether there is a path from u to v.
    bool hasPath(ConstVertex u, ConstVertex v) const         { return hasPath(u.descriptor(), v.descriptor()); }
    /// Indicate whether there is a path from vertex(id1) to vertex(id2).
    bool hasPath(ShapeID id1, ShapeID id2) const             { return hasPath(vertex(id1),  vertex(id2)); }
    /// Indicate whether there is a path from vertex(obj1) to vertex(obj2).
    bool hasPath(const Shape& obj1, const Shape& obj2) const { return hasPath(vertex(obj1), vertex(obj2)); }

    /// Find a path from u to v. If there is no path, the result is empty.
    std::vector<ConstVertex> findPath(VertexDescriptor u, VertexDescriptor v) const;
    /// Indicate whether there is a path from u to v. If there is no path, the result is empty.
    std::vector<ConstVertex> findPath(ConstVertex u, ConstVertex v) const         { return findPath(u.descriptor(), v.descriptor()); }
    /// Indicate whether there is a path from vertex(id1) to vertex(id2). If there is no path, the result is empty.
    std::vector<ConstVertex> findPath(ShapeID id1, ShapeID id2) const             { return findPath(vertex(id1),  vertex(id2)); }
    /// Indicate whether there is a path from vertex(obj1) to vertex(obj2). If there is no path, the result is empty.
    std::vector<ConstVertex> findPath(const Shape& obj1, const Shape& obj2) const { return findPath(vertex(obj1), vertex(obj2)); }


    // ITERATORATION RANGES

    /// Get the number of vertices in the graph.
    std::size_t numVertices() const { return boost::num_vertices(*this); }
    /// Get the number of edges in the graph.
    std::size_t numEdges() const { return boost::num_edges(*this); }


    /// Get a range over all vertices.
    ConstVertRange vertices() const { return ConstVertRange(this, boost::vertices(*this)); }
    VertRange vertices() { return VertRange(this, boost::vertices(*this)); }

    /// Get a range over all edges (between any vertices).
    ConstEdgeRange edges() const { return ConstEdgeRange(this, boost::edges(*this)); }
    EdgeRange edges() { return EdgeRange(this, boost::edges(*this)); }


    // VERTEX MODIFIERS

    /**
     * @brief Add a new vertex with the given attributes.
     * Attribute `objectID` is set to `id`.
     * Attribute `object` is not changed (nullptr by default).
     */
    Vertex addVertex(ShapeID id, const VertexAttrib& attr = {});
    /**
     * @brief Add a new vertex with the given attributes.
     * Attribute `objectID` is set to the object's ID.
     * Attribute `object` is set to point to the given object.
     */
    Vertex addVertex(Shape& obj, const VertexAttrib& attr = {});

    /// @brief Remove the given vertex (and all edges from or to v).
    /// This invalidates all vertex and edge descriptors and as well as iterators.
    void removeVertex(VertexDescriptor v);
    /// @brief Remove the given vertex (and all edges from or to v).
    /// This invalidates all vertex and edge descriptors and as well as iterators.
    void removeVertex(Vertex v) { removeVertex(v.descriptor()); }


    // EDGE MODIFIERS

    /**
     * @brief Add an edge from u to v with given attributes. If the edge already exists,
     * nothing is added, no attributes are changed, and the existing edge is returned.
     */
    Edge addEdge(VertexDescriptor u, VertexDescriptor v, const EdgeAttrib& attr = {});

    Edge addEdge(ConstVertex v1, ConstVertex v2, const EdgeAttrib& attr = {}) { return addEdge(v1.descriptor(), v2.descriptor(), attr); }

    /**
     * @brief Add an edge vertex(id1) to vertex(id2) with given attributes. If the edge already
     * exists, nothing is added, no attributes are changed, and the existing edge is returned.
     * @throw error::NoVertexwithID if there is no vertex with id1 or id2.
     */
    Edge addEdge(ShapeID id1, ShapeID id2, const EdgeAttrib& attr = {}) { return addEdge(vertex(id1), vertex(id2), attr); }
    /**
     * @brief Add an edge vertex(obj1) to vertex(obj2) with given attributes. If the edge already
     * exists, nothing is added, no attributes are changed, and the existing edge is returned.
     * @throw error::NoVertexwithID if there is no vertex with obj1 or obj2.
     */
    Edge addEdge(const Shape& obj1, const Shape& obj2, const EdgeAttrib& attr = {}) { return addEdge(vertex(obj1), vertex(obj2), attr); }



    /// Remove the edge e. This invalidates edge iterators.
    void removeEdge(EdgeDescriptor e) { boost::remove_edge(e, *this); }
    /// Remove the edge e. This invalidates edge iterators.
    void removeEdge(Edge e) { removeEdge(e.descriptor()); }


    // STRING OUTPUT

    /// Get a name of this graph (type) for use by operator<<() and str().
    std::string name() const { return serial::getTypeName(*this); }

    /**
     * @brief Get a string representation of the given vertex for usage in `str()`.
     * By default, returns `"[<vertex descriptor> (ID <object ID>)]"`.
     */
    virtual std::string strVertex(ConstVertex v) const;
    /**
     * @brief Get a string representation of the given edge for usage in `str()`.
     * By default, returns
     * `"[<source descriptor> (ID <source ID>) -> <target descriptor> (ID <target ID>)"`.
     */
    virtual std::string strEdge(ConstEdge e) const;
    /**
     * @brief Get a full string representation of the graph.
     * Vertices and edges are printed using strVertex() and strEdge().
     * Override these methods to customize the output by this method.
     */
    virtual std::string str() const;

    /// Writes a full string representation of the graph to os (by calling `graph.str()`).
    /// @see str()
    template <class VA, class EA, class GA>
    friend std::ostream& operator<<(std::ostream& os, const RelationGraph<VA, EA, GA>& graph);


    // OVERLOADS OF OPERATOR[]

    /// Get the vertex attributes of the given vertex.
    VertexAttrib& operator [](VertexDescriptor v) { return this->Boost::operator[](v); }
    const VertexAttrib& operator [](VertexDescriptor v) const { return this->Boost::operator[](v); }

    /// Get the vertex attributes of the given vertex.
    VertexAttrib& operator [](Vertex v) { return this->Boost::operator[](v.descriptor()); }
    const VertexAttrib& operator [](Vertex v) const { return this->Boost::operator[](v.descriptor()); }

    /// Get the vertex attributes of the vertex with the given ID.
    /// @throws error::NoVertexWithID if there is no vertex with the given ID.
    VertexAttrib& operator [](ShapeID id) { return (*this)[vertex(id).descriptor()]; }
    const VertexAttrib& operator [](ShapeID id) const { return (*this)[vertex(id).descriptor()]; }

    /// Get the edge attributes of the given edge.
    EdgeAttrib& operator [](EdgeDescriptor e) { return this->Boost::operator[](e);  }
    const EdgeAttrib& operator [](EdgeDescriptor e) const { return this->Boost::operator[](e);  }

    // operator[] can only take one argument => no operator for vertex-vertex

    /// Get the default graphviz writer.
    static graphviz::Writer<RelationGraph> getGraphvizWriter();


protected:

    /// Default graphviz vertex format. Labels a vertex by its object's tag.
    static graphviz::Attributes graphvizVertexFormat(ConstVertex vertex);
    static graphviz::Attributes graphvizVertexShapeFormat(ConstVertex vertex, const Shape* object);

    /// Default graphviz edge format (no-op).
    static graphviz::Attributes graphvizEdgeFormat(ConstEdge edge);
    static graphviz::Attributes graphvizEdgeShapeFormat(
            ConstEdge edge, const Shape* sourceobject, const Shape* targetObject);

    /// Default graphviz graph format.
    static graphviz::GraphAttributes graphvizGraphFormat(const RelationGraph& graph);

};

}

#include "RelationGraphImpl.h"
