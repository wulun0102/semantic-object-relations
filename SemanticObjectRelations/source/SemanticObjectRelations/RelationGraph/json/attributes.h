#pragma once

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>

#include <SimoxUtility/json/json.hpp>

namespace semrel
{
namespace json
{
    /// Serialize `attrib.objectID` into `j`.
    void to_json_base(nlohmann::json& j, const ShapeVertex& attrib);
    /// Deserialize `attrib.objectID` from `j`.
    void from_json_base(const nlohmann::json& j, ShapeVertex& attrib);
}

    /// @see json::to_json_base()
    void to_json(nlohmann::json& j, const ShapeVertex& attrib);
    /// @see json::from_json_base()
    void from_json(const nlohmann::json& j, ShapeVertex& attrib);

    /// Makes `j` an empty JSON object.
    void to_json(nlohmann::json& j, const NoAttrib& attrib);
    /// Does nothing.
    void from_json(const nlohmann::json& j, NoAttrib& attrib);

}
