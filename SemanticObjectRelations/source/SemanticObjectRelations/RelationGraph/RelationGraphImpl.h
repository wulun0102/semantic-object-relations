#pragma once

#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/visitors.hpp>

#include <SemanticObjectRelations/Visualization/SegmentColors.h>

#include "exceptions.h"
#include "RelationGraph.h" // just for auto-completion


namespace semrel
{

// CONSTRUCTORS

template <class VA, class EA, class GA>
RelationGraph<VA,EA,GA>::RelationGraph(const GraphAttrib& attribs) :
    Boost(attribs)
{}


template <class VA, class EA, class GA>
RelationGraph<VA,EA,GA>::RelationGraph(const std::vector<ShapeID>& objectIDs, const GraphAttrib& attribs) :
    Boost(objectIDs.size(), attribs)
{
    // Assign objects
    auto vit = this->vertices().begin();
    for (const auto& id : objectIDs)
    {
        vit->attrib().objectID = id;
        ++vit;
    }
}


template <class VA, class EA, class GA>
RelationGraph<VA,EA,GA>::RelationGraph(const ShapeList &objects, const GraphAttrib& attribs) :
    Boost(objects.size(), attribs)
{
    // Assign objects
    auto vit = this->vertices().begin();
    for (std::size_t i = 0; i < objects.size(); ++i)
    {
        const auto& object = objects[i];
        if (!object)
        {
            throw error::ShapePointerIsNull::InShapeList(i);
        }
        vit->attrib().objectID = object->getID();
        ++vit;
    }
}

template <class VA, class EA, class GA>
RelationGraph<VA,EA,GA>::RelationGraph(const ShapeMap& objects, const GraphAttrib& attribs) :
    Boost(objects.size(), attribs)
{
    // Assign objects
    auto vit = this->vertices().begin();
    for (const auto& [id, shapePtr] : objects)
    {
        vit->attrib().objectID = id;
        ++vit;
    }
}


// VERTEX ACCESS

template <class VA, class EA, class GA>
bool RelationGraph<VA,EA,GA>::hasVertex(ShapeID id) const
{
    for (auto v : this->vertices())
    {
        if (isVertexWithObjectID(v, id))
        {
            return true;
        }
    }
    return false;
}

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::vertex(std::size_t index) -> Vertex
{
    if (index >= numVertices())
    {
        throw error::NoVertexAtIndex(index, numVertices());
    }
    return Vertex(this, boost::vertex(index, *this));
}

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::vertex(ShapeID id) -> Vertex
{
    for (auto v : this->vertices())
    {
        if (v.objectID() == id)
        {
            return v;
        }
    }
    throw error::NoVertexWithID(id, this->str());
}


// EDGE ACCESS

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::edgeDescriptor(VertexDescriptor u, VertexDescriptor v) const
-> EdgeDescriptor
{
    EdgeDescriptor e;
    bool exists;
    boost::tie(e, exists) = boost::edge(u, v, *this);
    if (!exists)
    {
        throw error::NoEdgeBetween(u, v, this->str());
    }
    return e;
}


namespace  // anonymous namespace
{

/**
 * Boost BFS visitor to store whether the given target vertex was found.
 * Visitors are passed by value (not reference), so we need to reference an
 * extern bool (cannot use a member variable).
 */
template <typename Vertex>
class FindPathBfsVisitor : public boost::default_bfs_visitor
{
public:

    FindPathBfsVisitor(Vertex target, bool& pathFound) :
        target(target), pathFound(pathFound)
    {}

    template <typename _Vertex, typename Graph>
    void discover_vertex(_Vertex u, const Graph&) const
    {
        if (target == u)  pathFound = true;
    }

    Vertex target;  ///< The target vertex.
    bool& pathFound;  /// Result boolean.
};

}


template <class VA, class EA, class GA>
bool RelationGraph<VA,EA,GA>::hasPath(VertexDescriptor u, VertexDescriptor v) const
{
    bool pathFound = false;
    FindPathBfsVisitor<VertexDescriptor> visitor(v, pathFound);

    boost::breadth_first_search(*this, u, boost::visitor(visitor));

    return pathFound;
}


template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::findPath(VertexDescriptor u, VertexDescriptor v) const
-> std::vector<ConstVertex>
{
    // container for predecessors
    // entries == numVertices() correspond to "no parent"
    std::vector<VertexDescriptor> predecessors(numVertices(), numVertices());

    predecessors[u] = u; // map u to itself

    boost::breadth_first_search(
                *this, u, boost::visitor(boost::make_bfs_visitor(
                                             boost::record_predecessors(predecessors.data(),
                                                                        boost::on_tree_edge()))));
    if (predecessors[v] == numVertices())
    {
        return {}; // no path
    }


    // collect the path
    std::vector<ConstVertex> revPath { vertex(v) };  // start with v

    VertexDescriptor current = v;

    do
    {
        current = predecessors[current];  // move current

        // push current (first is predecessor of v, last is u)
        revPath.push_back(vertex(current));
    }
    while (predecessors[current] != current);  // stop when current == u

    // reverse the path
    std::reverse(revPath.begin(), revPath.end());

    return revPath;
}



// VERTEX MODIFIERS

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::addVertex(ShapeID id, const VA& attr) -> Vertex
{
    VertexDescriptor vd = boost::add_vertex(attr, *this);
    Vertex v = vertex(vd);
    v.attrib().objectID = id;
    return v;
}

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::addVertex(Shape& object, const VA& attr) -> Vertex
{
    VertexDescriptor vd = boost::add_vertex(attr, *this);
    Vertex v = vertex(vd);
    v.objectID() = object.getID();
    return v;
}


template <class VA, class EA, class GA>
void RelationGraph<VA,EA,GA>::removeVertex(typename RelationGraph<VA,EA,GA>::VertexDescriptor v)
{
    boost::clear_vertex(v, *this);
    boost::remove_vertex(v, *this);
}

template <class VA, class EA, class GA>
auto RelationGraph<VA,EA,GA>::addEdge(VertexDescriptor u, VertexDescriptor v, const EA& attr) -> Edge
{
    // If edge exists, do not add another.
    return this->edge(hasEdge(u, v) ? edgeDescriptor(u, v) : boost::add_edge(u, v, attr, *this).first);
}


// STRING OUTPUT

template <class VA, class EA, class GA>
std::string RelationGraph<VA,EA,GA>::strVertex(ConstVertex v) const
{
    std::stringstream ss;
    ss << "[" << v.descriptor() << " (ID " << v.objectID() << ")]";
    return ss.str();
}

template <class VA, class EA, class GA>
std::string RelationGraph<VA,EA,GA>::strEdge(ConstEdge e) const
{
    std::stringstream ss;
    ss << "["
       << e.sourceDescriptor() << " (ID " << e.sourceObjectID() << ")"
       << " -> "
       << e.targetDescriptor() << " (ID " << e.targetObjectID() << ")"
       << "]";
    return ss.str();
}

template <class VA, class EA, class GA>
std::string RelationGraph<VA,EA,GA>::str() const
{
    std::stringstream ss;
    ss << name() << " (" << numVertices() << " vertices, " << numEdges() << " edges)\n";
    ss << " == Vertices ==\n";
    for (auto v : vertices())
    {
        ss << " | " << strVertex(v) << "\n";
    }
    ss << " === Edges ====\n";
    for (auto v : vertices())
    {
        for (auto e : v.outEdges())
        {
            ss << " | " << strEdge(e) << "\n";
        }
    }
    return ss.str();
}

template <class VA, class EA, class GA>
graphviz::Writer<RelationGraph<VA,EA,GA>> RelationGraph<VA,EA,GA>::getGraphvizWriter()
{
    return { graphvizVertexFormat, graphvizVertexShapeFormat,
                graphvizEdgeFormat, graphvizEdgeShapeFormat, graphvizGraphFormat };
}


template <class VA, class EA, class GA>
graphviz::Attributes RelationGraph<VA,EA,GA>::graphvizVertexFormat(ConstVertex vertex)
{
    const ShapeID id = vertex.objectID();
    return {
        { "label", std::to_string(id) },
        { "color", "#" + SegmentColors::getColorCode(id) },
        { "style", "filled" },
    };
}

template <class VA, class EA, class GA>
graphviz::Attributes RelationGraph<VA,EA,GA>::graphvizVertexShapeFormat(ConstVertex, const Shape* object)
{
    return {
        { "label", object->tag() },
        { "color", "#" + SegmentColors::getColorCode(object->getID()) },
        { "style", "filled" },
    };
}

template <class VA, class EA, class GA>
graphviz::Attributes RelationGraph<VA,EA,GA>::graphvizEdgeFormat(ConstEdge)
{
    return {};
}

template <class VA, class EA, class GA>
graphviz::Attributes RelationGraph<VA,EA,GA>::graphvizEdgeShapeFormat(ConstEdge, const Shape*, const Shape*)
{
    return {};
}

template <class VA, class EA, class GA>
graphviz::GraphAttributes RelationGraph<VA,EA,GA>::graphvizGraphFormat(const RelationGraph<VA,EA,GA>&)
{
    return {};
}


// OPERATORS

template <class VA, class EA, class GA>
std::ostream& operator<<(std::ostream& os, const RelationGraph<VA,EA,GA>& graph)
{
    os << graph.str();
    return os;
}

}
