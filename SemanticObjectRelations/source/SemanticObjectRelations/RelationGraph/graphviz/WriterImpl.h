#pragma once

#include "Writer.h"

#include <boost/graph/graphviz.hpp>

#include <iosfwd>

namespace semrel::graphviz
{

    template <class RelationGraphT>
    Writer<RelationGraphT>::Writer(WriterMode mode) :
        mode(mode)
    {}

    template<class RelationGraphT>
    Writer<RelationGraphT>::Writer(Writer::VertexFormat vertexFormat, Writer::EdgeFormat edgeFormat, Writer::GraphFormat graphFormat, WriterMode mode) :
        vertexFormat(vertexFormat), edgeFormat(edgeFormat),
        graphFormat(graphFormat), mode(mode)
    {}

    template<class RelationGraphT>
    Writer<RelationGraphT>::Writer(Writer::VertexShapeFormat vertexShapeFormat, Writer::EdgeShapeFormat edgeShapeFormat, Writer::GraphFormat graphFormat, WriterMode mode) :
        vertexShapeFormat(vertexShapeFormat), edgeShapeFormat(edgeShapeFormat),
        graphFormat(graphFormat), mode(mode)
    {}

    template<class RelationGraphT>
    Writer<RelationGraphT>::Writer(Writer::VertexFormat vertexFormat, Writer::VertexShapeFormat vertexShapeFormat, Writer::EdgeFormat edgeFormat, Writer::EdgeShapeFormat edgeShapeFormat, Writer::GraphFormat graphFormat, WriterMode mode) :
        vertexFormat(vertexFormat), vertexShapeFormat(vertexShapeFormat),
        edgeFormat(edgeFormat), edgeShapeFormat(edgeShapeFormat),
        graphFormat(graphFormat), mode(mode)
    {}

    template <class RelationGraphT>
    bool Writer<RelationGraphT>::write(const RelationGraphT& graph, const std::string& filename) const
    {
        return this->write(filename, [&](const path& filenameDot)
        {
            return writeDot(filenameDot, graph);
        });
    }


    template <class RelationGraphT>
    bool Writer<RelationGraphT>::write(
            const RelationGraphT& graph, const ShapeMap& shapes, const std::string& filename) const
    {
        return this->write(filename, [&](const path& filenameDot)
        {
            return writeDot(filenameDot, graph, shapes);
        });
    }

    template <class RelationGraphT>
    bool Writer<RelationGraphT>::write(const path& filename, std::function<bool(const path&)> writeDotFn) const
    {
        namespace fs = std::filesystem;

        fs::path filenameDot = filename;
        filenameDot.replace_extension(".dot");

        fs::path filenameImg = filename;
        if (!filenameImg.has_extension())
        {
            filenameImg = filenameImg.string() + ".png";
        }

        bool success = true;
        success &= writeDotFn(filenameDot);

        // Generate image (skip if dot file could not be written).
        if (success && hasFlag(mode, WriterMode::PNG))
        {
            success &= writeImage(filenameImg.string(), filenameDot.string());
        }

        if (!hasFlag(mode, WriterMode::DOT))
        {
            // Remove dot file (ignore possible error).
            fs::remove(filenameDot);
        }

        // For now, be silent on errors.
        return success;
    }


    template <class RelationGraphT>
    bool Writer<RelationGraphT>::writeDot(
        const path& dotFilename, const RelationGraphT& graph) const
    {
        std::ofstream dotStream(dotFilename);
        if (!dotStream.is_open())
        {
            return false;
        }

        // Bind stored formats to given graph instance
        // Signature as expected by write_graphviz: vw(std::ostream&, VertexDescriptor)

        auto boundVertexFormat = [this, &graph](std::ostream & os, VertexDescriptor descriptor)
        {
            os << vertexFormat(ConstVertex(&graph, descriptor));
        };
        auto boundEdgeFormat = [this, &graph](std::ostream & os, EdgeDescriptor descriptor)
        {
            os << edgeFormat(ConstEdge(&graph, descriptor));
        };
        auto boundGraphFormat = [this, &graph](std::ostream & os)
        {
            os << graphFormat(graph);
        };

        boost::write_graphviz(dotStream, graph, boundVertexFormat, boundEdgeFormat, boundGraphFormat);
        return true;
    }

    template<class RelationGraphT>
    bool Writer<RelationGraphT>::writeDot(
            const path& dotFilename, const RelationGraphT& graph, const ShapeMap& shapes) const
    {
        std::ofstream dotStream(dotFilename);
        if (!dotStream.is_open())
        {
            return false;
        }

        // Bind stored formats to given graph instance
        // Signature as expected by write_graphviz: vw(std::ostream&, VertexDescriptor)

        auto boundVertexFormat = [this, &graph, &shapes](std::ostream & os, VertexDescriptor descriptor)
        {
            ConstVertex vertex(&graph, descriptor);
            os << vertexShapeFormat(vertex, shapes.at(vertex.objectID()));
        };
        auto boundEdgeFormat = [this, &graph, &shapes](std::ostream & os, EdgeDescriptor descriptor)
        {
            ConstEdge edge(&graph, descriptor);
            os << edgeShapeFormat(edge, shapes.at(edge.sourceObjectID()), shapes.at(edge.targetObjectID()));
        };
        auto boundGraphFormat = [this, &graph](std::ostream & os)
        {
            os << graphFormat(graph);
        };

        boost::write_graphviz(dotStream, graph, boundVertexFormat, boundEdgeFormat, boundGraphFormat);
        return true;
    }

    template <class RelationGraphT>
    bool Writer<RelationGraphT>::writeImage(
        const path& imgFilename, const path& dotFilename) const
    {
        // check whether 'dot' command is available (https://stackoverflow.com/a/40182574)
        if (system("which dot > /dev/null 2>&1") != 0)
        {
            // dot not available
            return false;
        }

        // dot available
        std::stringstream dotCommand;
        dotCommand << "dot -Tpng " << dotFilename << " -o " << imgFilename;

        // run command
        int r = system(dotCommand.str().c_str());
        return r == 0;
    }

}
