#pragma once

#include <map>
#include <string>


namespace semrel { namespace graphviz
{
    /// Name-value pairs, e.g. ("label", "node1"), ("fillcolor", "red").
    using Attributes = std::map<std::string, std::string>;
    /// A name-value pair.
    using Attribute = Attributes::value_type;


    /// Global format attributes.
    struct GraphAttributes
    {
        Attributes graph;   ///< Graph attributes.
        Attributes vertex;  ///< Global vertex attributes.
        Attributes edge;    ///< Global edge attributes.
    };
    
    
    /// Writes `key="value"` to os.
    std::ostream& operator<<(std::ostream& os, const Attribute& rhs);
    
    /// Writes `[ key1="value1" key2="value2" ]` to os.
    std::ostream& operator<<(std::ostream& os, const Attributes& rhs);
    
    /// Writes the graph, vertex and edge attributes to os.
    std::ostream& operator<<(std::ostream& os, const GraphAttributes& rhs);
    
    
}}
