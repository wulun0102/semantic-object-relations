#include "exceptions.h"


namespace semrel::error
{

    SemanticObjectRelationsError::SemanticObjectRelationsError(const std::string& msg) :
        std::runtime_error(msg)
    {
    }

}


