#pragma once

#include "Visualizer.h"

namespace semrel
{

class BoxRansacVisualizer : public Visualizer
{
    
public:
    
    static BoxRansacVisualizer& get();
    
    
    BoxRansacVisualizer() : Visualizer("ShapeExtraction") {}


    // Box Ransac Life visualization
    using Visualizer::clearLayer;
    
    void shortSleep();
    void longSleep();
    
    
    void drawPointCloudLife(const std::vector<Eigen::Vector3f>& pointcloud);
    
    void drawMSS(const std::vector<Eigen::Vector3f>& mss);
    void drawAxes(const Eigen::Matrix3f& axes, const Eigen::Vector3f& at);
    void drawVisiblePlanes(const std::vector<Hyperplane3f>& planes, const Eigen::Vector3f& at);
    void drawVisiblePlaneInliers(const std::vector<Eigen::Vector3f> (& planeInliers)[3]);

    
    void drawFittingPlane(const Hyperplane3f& planes, const Eigen::Vector3f& at, int iRGB);
    void drawInliers(const std::vector<Eigen::Vector3f>& inliers, int iRGB = 0);

    void drawFittingLine(const ParametrizedLine3f& line, const Eigen::Vector3f& at, int iRGB);
    void drawFittingLine(const ParametrizedLine3f& line, float minT, float maxT, int iRGB);
    
    void drawBoxCandidate(const Box& box);
    
    
};

}
