#include "SegmentColors.h"

#include <pcl/point_types.h>  // for pcl::RGB
#include <pcl/common/colors.h>

#include <iomanip>


using namespace semrel;


SegmentColors::SegmentColors()
{
}

DrawColor SegmentColors::getColor(ShapeID id, float alpha)
{
    pcl::RGB rgb = pcl::GlasbeyLUT::at(static_cast<unsigned int>(id) % pcl::GlasbeyLUT::size());
    
    float scale = 1 / 255.f;
    return { rgb.r * scale, rgb.g * scale, rgb.b * scale, alpha };
}

std::string SegmentColors::getColorCode(ShapeID id)
{
    const DrawColor color = getColor(id);
    std::stringstream ss;
    ss << std::uppercase << std::hex << std::setw(2) << std::setfill ('0') << static_cast<int>(color.r * 255);
    ss << std::uppercase << std::hex << std::setw(2) << std::setfill ('0') << static_cast<int>(color.g * 255);
    ss << std::uppercase << std::hex << std::setw(2) << std::setfill ('0') << static_cast<int>(color.b * 255);
    return ss.str();
}

DrawColor SegmentColors::fromHex(const std::string& hexColor, float alpha)
{
    DrawColor color;
    
    color.r = std::stoi(hexColor.substr(0, 2), nullptr, 16) / 255.f;
    color.g = std::stoi(hexColor.substr(2, 2), nullptr, 16) / 255.f;
    color.b = std::stoi(hexColor.substr(4, 2), nullptr, 16) / 255.f;
    
    color.a = alpha;
    
    return color;
}
