#include "BoxRansacVisualizer.h"

#include <chrono>
#include <thread>

using namespace semrel;


BoxRansacVisualizer& BoxRansacVisualizer::get()
{
    static BoxRansacVisualizer instance;
    return instance;
}

static void sleepMS(int ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void BoxRansacVisualizer::shortSleep()
{
    draw(VisuLevel::LIVE_VISU, []
    {
        sleepMS(50);
    });
}

void BoxRansacVisualizer::longSleep()
{
    draw(VisuLevel::LIVE_VISU, []
    {
        sleepMS(500);
    });
}

void BoxRansacVisualizer::drawPointCloudLife(const std::vector<Eigen::Vector3f>& pointcloud)
{
    draw(VisuLevel::LIVE_VISU, [&] 
    {
        this->drawPointCloud(VisuLevel::LIVE_VISU, "BoxRansac_Pointcloud", pointcloud);
    });
}

void BoxRansacVisualizer::drawMSS(const std::vector<Eigen::Vector3f>& mss)
{
    draw(VisuLevel::LIVE_VISU, [&] 
    {
        DrawColor color123 = {1., 0., 0., 1.};
        DrawColor color45 = {0., 0., 1., 1.};
    
        for (std::size_t i = 0; i < mss.size(); ++i)
        {
            DrawColor color = color123;
            if (i >= 3)
            {
                color = color45;
            }
    
            std::stringstream ss;
            ss << "MSS_" << i;
            drawImportantPoint(ss.str(), mss[i], 15.f, color);
        }
    });
    
}

void BoxRansacVisualizer::drawAxes(const Eigen::Matrix3f& axes, const Eigen::Vector3f& at)
{
    draw(VisuLevel::LIVE_VISU, [&] 
    {
        float length = 500.f;
        float width = 2.5f;
    
        impl().drawArrow(visuID("normal1"), at, axes.col(0), length, width, redGreenBlue(0));
        impl().drawArrow(visuID("normal2"), at, axes.col(1), length, width, redGreenBlue(1));
        impl().drawArrow(visuID("normal3"), at, axes.col(2), length, width, redGreenBlue(2));
    });
    
}

void BoxRansacVisualizer::drawVisiblePlanes(const std::vector<Hyperplane3f>& planes, const Eigen::Vector3f& at)
{
    draw(VisuLevel::LIVE_VISU, [&]
    {
        for (std::size_t i = 0; i < planes.size(); ++i)
        {
            drawFittingPlane(planes[i], at, int(i));
        }
    });
}

void BoxRansacVisualizer::drawVisiblePlaneInliers(const std::vector<Eigen::Vector3f> (& planeInliers)[3])
{
    draw(VisuLevel::LIVE_VISU, [&] 
    {
        for (std::size_t i = 0; i < 3; ++i)
        {
            drawInliers(planeInliers[i], int(i));
        }
    });
}

void BoxRansacVisualizer::drawFittingPlane(const Hyperplane3f& plane, const Eigen::Vector3f& at, int iRGB)
{
    std::stringstream ss;
    ss << "plane_" << iRGB;
    drawPlane(VisuLevel::LIVE_VISU, ss.str(), plane, at, 750, 400, redGreenBlue(iRGB));
}

void BoxRansacVisualizer::drawInliers(const std::vector<Eigen::Vector3f>& inliers, int iRGB)
{
    draw(VisuLevel::LIVE_VISU, [&] 
    {
        std::stringstream ss;
        ss << "inliers_" << iRGB;
        drawPointCloud(VisuLevel::LIVE_VISU, ss.str(), inliers, 2.0, redGreenBlue(iRGB));
    });
    
}

void BoxRansacVisualizer::drawFittingLine(const ParametrizedLine3f& line, const Eigen::Vector3f& at, int iRGB)
{
    draw(VisuLevel::LIVE_VISU, [&]
    {
        DrawColor color = redGreenBlue(iRGB);
        float length = 1000.;
        float width = 0.5;
        
        Eigen::Vector3f atLine = line.projection(at);
        
        impl().drawLine(visuID(iRGB, "line_"), 
                    atLine - length/2 * line.direction(),
                    atLine + length/2 * line.direction(),
                    width, color);
    });
}

void BoxRansacVisualizer::drawFittingLine(const ParametrizedLine3f& line, float minT, float maxT, int iRGB)
{
    draw(VisuLevel::LIVE_VISU, [&]
    {
        DrawColor color = redGreenBlue(iRGB);
        float width = 10.;
    
        impl().drawLine(visuID(iRGB, "line_"), line.pointAt(minT), line.pointAt(maxT), width, color);
    });
}

void BoxRansacVisualizer::drawBoxCandidate(const Box& box)
{
    drawBox(VisuLevel::LIVE_VISU, box);
}


