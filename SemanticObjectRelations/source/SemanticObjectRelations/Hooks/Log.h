#pragma once

#include <memory>
#include <sstream>

namespace semrel
{


/// The verbosity levels, in roughly descending order of relevance.
enum class LogLevel {
    DEBUG,     ///< For debugging.
    VERBOSE,   ///< Information which is relevant only on special occasions.
    INFO,      ///< "Standard level". Interesting information for normal usage.
    IMPORTANT, ///< Non-errors with high relevance.
    WARNING,   ///< Things the user should be aware of, but are somehow handled.
    ERROR,     ///< A true error.
};


/// Meta information passed to the logger.
struct LogMetaInfo
{
    LogLevel level;   ///< The verbositiy level.
    std::string file; ///< The current file.
    int line;         ///< The current line.
    std::string func; ///< The current function.
};


/**
 * @brief The LogInterface. 
 * 
 * To use your own logging facility, derive from this interface and implement
 * the virtual method `log()`. Then, use `LogInterface::setImplementation()`
 * to set your logging implementation.
 */
class LogInterface
{

public:

    /// Get the current LogInterface implementation.
    static LogInterface& get();

    /// Set the LogInterface implementation.
    static void setImplementation(std::shared_ptr<LogInterface> implementation);

    static LogLevel getMinimumLogLevel();
    static void setMinimumLogLevel(LogLevel level);

    
public:
    
    /// Constructor.
    LogInterface();
    /// Virtual destructor.
    virtual ~LogInterface();

    /**
     * @brief Log the given message under the given meta information.
     * This method is called once for every chain of stream operators.
     * That is, in the code snippet
     * @code
     * LOG_INFO << "The value of i is " << i;
     * LOG_INFO << "And the " << "value" << " of x is: << x << ", however.";
     * @endcode
     * this method will be called two times, once for the chain including i
     * and once for the other involving x.
     *
     * You may want to flush your stream or write std::endl to it.
     */
    virtual void log(LogMetaInfo info, const std::string& message) = 0;

    
private:

    /**
     * @brief The currently set implementation.
     * This defaults to a StdLog. Set it via LogInterface::setImplementation().
     */
    static std::shared_ptr<LogInterface> implementation;

    static LogLevel minimumLevel;
    
};


/**
 * @brief Handler for log instructions.
 * Used via the macros SR_LOG_[VERBOSITY], e.g. SR_LOG_INFO.
 *
 * The class Log uses the RAII idiom: Each use of a log macro will
 * instantiate an instance of Log with respective meta information. This
 * instance will accept anything streamed to it, and will accumulate it
 * (in a std::stringstream) until it is destroyed (at the end of the stream
 * chain). It will then call LogInterface::get()->log() with its meta
 * information and accumulated message.
 */
class Log
{
public:
    /// Construct with given meta information.
    Log(LogMetaInfo info);
    /// Passes the accumulated message and meta info to the logger.
    ~Log();

    /// Passes anything to a std::stringstream.
    template <typename T>
    Log& operator << (const T& t)
    {
        message << t;
        return *this;
    }

private:
    /// The meta information.
    LogMetaInfo info;
    /// The message stream.
    std::stringstream message;
};


// Macros for usage like: SR_LOG_INFO << "i = " << i;

#define SR_LOG_ERROR ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::ERROR, __FILE__, __LINE__, __func__})
#define SR_LOG_WARNING ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::WARNING, __FILE__, __LINE__, __func__})
#define SR_LOG_IMPORTANT ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::IMPORTANT, __FILE__, __LINE__, __func__})
#define SR_LOG_INFO ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::INFO, __FILE__, __LINE__, __func__})
#define SR_LOG_VERBOSE ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::VERBOSE, __FILE__, __LINE__, __func__})
#define SR_LOG_DEBUG ::semrel::Log(::semrel::LogMetaInfo{::semrel::LogLevel::DEBUG, __FILE__, __LINE__, __func__})


/**
 * @brief Ignores everything streamed to it. Can be used to disable any logging.
 */
class NoLog : public LogInterface
{
public:
    NoLog();
    virtual void log(LogMetaInfo info, const std::string& message) override;
};


/**
 * @brief Streams log messages to std::cout or std::cerr (for errors).
 * Adds some information depending on the log level.
 */
class StdLog : public LogInterface
{
public:
    StdLog();
    virtual void log(LogMetaInfo info, const std::string& message) override;
};

}
