#include "Log.h"

#include <iostream>

namespace semrel
{

std::shared_ptr<LogInterface> LogInterface::implementation = std::make_shared<StdLog>();
LogLevel LogInterface::minimumLevel = LogLevel::DEBUG;


LogInterface& LogInterface::get()
{
    return *implementation;
}

void LogInterface::setImplementation(std::shared_ptr<LogInterface> implementation)
{
    LogInterface::implementation = implementation;
}

LogLevel LogInterface::getMinimumLogLevel()
{
    return LogInterface::minimumLevel;
}

void LogInterface::setMinimumLogLevel(LogLevel level)
{
    LogInterface::minimumLevel = level;
}

LogInterface::LogInterface() 
{
}

LogInterface::~LogInterface()
{
}

Log::Log(LogMetaInfo info) : info(info) 
{
    
}

Log::~Log()
{
    if (info.level >= LogInterface::getMinimumLogLevel())
    {
        LogInterface::get().log(info, message.str());
    }
}

NoLog::NoLog()
{
}

void NoLog::log(LogMetaInfo info, const std::string& message)
{
    // do nothing
    (void) info;
    (void) message;
}

StdLog::StdLog()
{
}

void StdLog::log(LogMetaInfo info, const std::string& message)
{
    switch (info.level) {
    case LogLevel::DEBUG:
        std::cout << "[DEBUG " << info.func << "]: "
                  << message << std::endl;
        break;
    case LogLevel::VERBOSE:
    case LogLevel::INFO:
        std::cout << message << std::endl;
        break;
    case LogLevel::IMPORTANT:
        std::cout << "[IMPORTANT] " << message << std::endl;
        break;
    case LogLevel::WARNING:
        std::cout << "[WARNING] " << message << std::endl;
        break;
    case LogLevel::ERROR:
        std::cerr << "[" << info.func << " line " << info.line << "] ERROR: "
                  << message << std::endl;
        break;
    default:
        throw std::logic_error("Unexpected value of enum Level");
        break;
    }
}

}
