#include "ActReasoning.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/SupportAnalysisVisualizer.h>

#include "SupportAnalysis.h"


namespace semrel
{

ActReasoning::ActReasoning(const Eigen::Vector3f& gravity,
                           float verticalPlaneAngleMax,
                           bool useNormalsForVerticalPlane)
    : vertSepPlaneAngleMax(verticalPlaneAngleMax)
    , useNormalsForVerticalPlane(useNormalsForVerticalPlane)
{
    setGravityVector(gravity);
}

ActReasoning::Result ActReasoning::decideAct(const Shape &objectA, const Shape &objectB,
        const ContactPointList& contactPoints) const
{
    // construct separating plane
    Hyperplane3f sepPlane = constructSeparatingPlane(contactPoints);

    float upDotNormal = up.dot(sepPlane.normal());

    // if necessary, flip plane so normal points upwards
    if (upDotNormal < 0)
    {
        sepPlane = Hyperplane3f(- sepPlane.normal(), - sepPlane.offset());
        upDotNormal *= -1;
    }

    SupportAnalysisVisualizer::get().drawSeparatingPlane(sepPlane, getCentroid(contactPoints),
                                                         objectA.getID(), objectB.getID());

    // check for vertical plane <=> normal is horizontal <=> perpendicular to up

    // up * normal  = cos(theta)
    // => theta = arccos(up * normal) in [0 .. PI] (up * normal is positive)
    // theta is 90 deg if normal horizontal, and 0 if normal vertical

    float theta = std::acos(upDotNormal) * 180. / M_PI;

    float separatingPlaneAngle = 90 - theta;

    if (separatingPlaneAngle <= vertSepPlaneAngleMax)
    {
        // => vertical plane
        return VERT_SEP_PLANE;
    }


    // decide whether A or B is up
    float signedDistA = sepPlane.signedDistance(objectA.getPosition());
    float signedDistB = sepPlane.signedDistance(objectB.getPosition());

    if (useNormalsForVerticalPlane)
    {
        if (signedDistA > 0 && signedDistB < 0)
        {
            return A_ACTS;
        }
        else if (signedDistA < 0 && signedDistB > 0)
        {
            return B_ACTS;
        }
        else
        {
            // both objects centers are on the same side of the plane
            // this can happen due to the contact margin
            // if the objects touch at a point far away of the center

            // in these cases, just use the contact normals
            // I think the normals always align

            int votesA = 0;
            int votesB = 0;
            for (const ContactPoint& cp : contactPoints)
            {
                Eigen::Vector3f normalOnB = cp.getNormalOnB();

                // check wether normal is aligned with plane normal (points upwards)
                if (normalOnB.dot(sepPlane.normal()) > 0)
                {
                    // aligned => normalOnB points to upper side => A is up
                    votesA++;
                }
                else
                {
                    votesB++;
                }
            }
            if (votesA > votesB)
            {
                return A_ACTS;
            }
            else if (votesA < votesB)
            {
                return B_ACTS;
            }
            else
            {
                // same votes => vertical sep plane
                // although I dont think this occurs because the normals are usually aligned
                return VERT_SEP_PLANE;
            }
        }
    }
    else
    {
        if (signedDistA > signedDistB)
        {
            return A_ACTS;
        }
        else if (signedDistA < signedDistB)
        {
            return B_ACTS;
        }
        else
        {
            return VERT_SEP_PLANE;
        }
    }
}

Hyperplane3f ActReasoning::constructSeparatingPlane(const ContactPointList& contactPoints)
{
    std::size_t numContacts = contactPoints.size();

    switch (numContacts)
    {
    case 0:
        throw std::logic_error("Got contact edge with 0 contact points.");

    case 1:
    {
        const ContactPoint& cp = contactPoints.front();
        return Hyperplane3f(cp.getNormalOnB(), cp.getPosition());
    }

    case 2:
    {
        const ContactPoint& cp1 = contactPoints[0];
        const ContactPoint& cp2 = contactPoints[1];

        Eigen::Vector3f normal = (cp1.getNormalOnB() + cp2.getNormalOnB()).normalized();
        Eigen::Vector3f position = 0.5 * (cp1.getPosition() + cp2.getPosition());

        return Hyperplane3f(normal, position);
    }

    case 3:
    {
        return Hyperplane3f::Through(contactPoints[0].getPosition(),
                contactPoints[1].getPosition(),
                contactPoints[2].getPosition());
    }

    default: // > 3
    {
        SR_LOG_DEBUG << numContacts << " contacts => Use SVD";

        // compute centroid
        Eigen::Vector3f centroid(0, 0, 0);
        for (auto& cp : contactPoints)
        {
            centroid += cp.getPosition();
        }
        centroid /= numContacts;

        /* find SVD of A:
             * [                   ]
             * [ (c1-c) ... (cm-c) ] (3xm)
             * [                   ]
             *
             * Best normal is left singular vector of least singular value
             * according to: https://math.stackexchange.com/a/99317
             */

        // build A
        Eigen::MatrixXf A(3, numContacts);
        for (std::size_t i = 0; i < numContacts; ++i)
        {
            A.col(i) = contactPoints[i].getPosition() - centroid;
        }

        // compute SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(A, Eigen::ComputeFullU);

        // A is 3xm => U is 3x3, V is mxm
        Eigen::Vector3f normal = svd.matrixU().col(2);

        return Hyperplane3f(normal, centroid);
    }
    }
}


void ActReasoning::setGravityVector(const Eigen::Vector3f& gravity)
{
    this->up = - gravity.normalized();
}

Eigen::Vector3f ActReasoning::getGravityVector() const
{
    return -up;
}

void ActReasoning::setVertSepPlaneAngleMax(float angleMax)
{
    this->vertSepPlaneAngleMax = angleMax;
}

float ActReasoning::getVertSepPlaneAngleMax() const
{
    return vertSepPlaneAngleMax;
}

Eigen::Vector3f ActReasoning::getCentroid(const ContactPointList& contactPoints)
{
    Eigen::Vector3f centroid(0, 0, 0);
    for (const auto& cp : contactPoints)
    {
        centroid += cp.getPosition();
    }
    centroid /= contactPoints.size();
    return centroid;
}

}

