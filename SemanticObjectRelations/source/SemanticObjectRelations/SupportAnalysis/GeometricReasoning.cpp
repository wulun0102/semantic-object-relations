#include "GeometricReasoning.h"

#include "SupportAnalysis.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/SupportAnalysisVisualizer.h>


namespace semrel
{

GeometricReasoning::GeometricReasoning(bool verticalSeparatingPlaneAssumeSupport) :
    vertSepPlaneAssumeSupport(verticalSeparatingPlaneAssumeSupport)
{
}

SupportGraph GeometricReasoning::performGeometricReasoning(
        const ShapeMap& objects, const ContactGraph* contactGraph) const
{
    const ContactGraph& usedContactGraph = contactGraph ? *contactGraph
                                                        : contactDetection.computeContacts(objects);

    SR_LOG_VERBOSE << "Building Act Graph";
    SupportGraph actGraph = buildActGraph(usedContactGraph, objects);

    SR_LOG_VERBOSE << "Act Graph: " << actGraph;

    return actGraph;
}

SupportGraph GeometricReasoning::performGeometricReasoning(
        const ShapeList& objects, const ContactGraph* contactGraph) const
{
    return performGeometricReasoning(toShapeMap(objects), contactGraph);
}


SupportGraph GeometricReasoning::buildActGraph(const ContactGraph& cg,
                                               const ShapeMap& objects) const
{
    SR_LOG_DEBUG << "Making support graph for objects " << objects
                 << " (" << objects.size() << " vertices)";

    SupportGraph act(objects);

    // For each edge in cg, add an act edge.

    SR_LOG_DEBUG << "Examining contacts ...";

    for (auto edge : cg.edges())
    {
        // Get objects
        const Shape& objA = *objects.at(edge.source().objectID());
        const Shape& objB = *objects.at(edge.target().objectID());

        ContactPointList contactPoints = edge.attrib().contactPoints;

        // Find corresponding vertices in act graph.
        SupportGraph::Vertex actVertexA = act.vertex(objA.getID());
        SupportGraph::Vertex actVertexB = act.vertex(objB.getID());

        SupportGraph::Vertex vertexActing;
        SupportGraph::Vertex vertexActed;
        bool verticalSepPlane = false;

        // Decide act relation.
        ActReasoning::Result actResult = actReasoning.decideAct(objA, objB, contactPoints);

        switch (actResult)
        {
        case ActReasoning::A_ACTS:
            SR_LOG_DEBUG << objA.tag() << " acts on " << objB.tag();
            vertexActing = actVertexA;
            vertexActed = actVertexB;
            break;
        case ActReasoning::B_ACTS:
            SR_LOG_DEBUG << objB.tag() << " acts on " << objA.tag();
            vertexActing = actVertexB;
            vertexActed = actVertexA;
            break;
        case ActReasoning::VERT_SEP_PLANE:
            SR_LOG_DEBUG << "Vertical separating plane between " << objA.tag()
                         << " and " << objB.tag();
            verticalSepPlane = true;
            break;
        default:
            throw std::logic_error("Unknown value of enum ActReasoning::Result.");
            break;
        }

        // Add edge(s) to act graph.

        SupportGraph::Edge newEdge;
        if (!verticalSepPlane)
        {
            newEdge = act.addEdge(vertexActing, vertexActed);
            newEdge.attrib().verticalSeparatingPlane = false;
        }
        else if (vertSepPlaneAssumeSupport)
        {
            // A -> B
            newEdge = act.addEdge(actVertexA, actVertexB);
            newEdge.attrib().verticalSeparatingPlane = true;

            // B -> A
            newEdge = act.addEdge(actVertexB, actVertexA);
            newEdge.attrib().verticalSeparatingPlane = true;
        }
    }

    SupportAnalysisVisualizer::get().drawActGraph(act, objects);

    return act;
}

void GeometricReasoning::setVertSepPlaneAssumeSupport(bool assumeSupport)
{
    this->vertSepPlaneAssumeSupport = assumeSupport;
}

bool GeometricReasoning::getVertSepPlaneAssumeSupport() const
{
    return vertSepPlaneAssumeSupport;
}

void GeometricReasoning::setContactMargin(float margin)
{
    contactDetection.setMargin(margin);
}

float GeometricReasoning::getContactMargin() const
{
    return contactDetection.getMargin();
}

void GeometricReasoning::setGravityVector(const Eigen::Vector3f& gravity)
{
    actReasoning.setGravityVector(gravity);
}

Eigen::Vector3f GeometricReasoning::getGravityVector() const
{
    return actReasoning.getGravityVector();
}

void GeometricReasoning::setVertSepPlaneAngleMax(float angleMax)
{
    actReasoning.setVertSepPlaneAngleMax(angleMax);
}

float GeometricReasoning::getVertSepPlaneAngleMax() const
{
    return actReasoning.getVertSepPlaneAngleMax();
}

}

