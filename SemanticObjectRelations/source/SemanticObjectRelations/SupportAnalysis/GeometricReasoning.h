#pragma once

#include <SemanticObjectRelations/ContactDetection/ContactDetection.h>

#include "ActReasoning.h"
#include "SupportGraph.h"


namespace semrel
{

/**
 * @brief Geometric reasoning determines the act relation between each
 * pair of objects.
 *
 * @param Vertical separating plane - Assume support: (default: false)
 *      If true, edges are added if the separating plane is vertical.
 *
 * @param @see ContactDetection
 * @param @see ActReasoning
 */
class GeometricReasoning
{
public:

    /// Constructor.
    GeometricReasoning(bool vertSepPlaneAssumeSupport = false);


    /// Compute contacts and act relation.
    SupportGraph performGeometricReasoning(const ShapeMap& objects,
                                           const ContactGraph* contactGraph = nullptr) const;
    SupportGraph performGeometricReasoning(const ShapeList& objects,
                                           const ContactGraph* contactGraph = nullptr) const;


    /// Set whether or not to add edges for vertical separating planes.
    void setVertSepPlaneAssumeSupport(bool assumeSupport);
    bool getVertSepPlaneAssumeSupport() const;

    /// @see ContactDetection
    void setContactMargin(float margin);
    float getContactMargin() const;
    /// @see ActReasoning
    void setGravityVector(const Eigen::Vector3f& gravity);
    Eigen::Vector3f getGravityVector() const;
    /// @see ActReasoning
    void setVertSepPlaneAngleMax(float angleMax);
    float getVertSepPlaneAngleMax() const;


private:

    /// Build the act graph from the contact graph.
    SupportGraph buildActGraph(const semrel::ContactGraph& contactGraph,
                               const ShapeMap& objects) const;

    /// The contact detection.
    ContactDetection contactDetection;

    /// The act reasoning.
    ActReasoning actReasoning;


    /// Whether or not to add edges for vertical separating planes.
    bool vertSepPlaneAssumeSupport;

};

}
