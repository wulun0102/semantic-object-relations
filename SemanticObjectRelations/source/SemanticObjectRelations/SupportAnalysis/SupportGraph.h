#pragma once

#include <SemanticObjectRelations/RelationGraph/graphviz.h>
#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>


namespace semrel
{

    struct SupportVertex : public ShapeVertex
    {
        /// Uncertainty detection data
        struct UD
        {
            /// Whether UD was enabled or not.
            bool enabled = false;
            /// Support area ratio of the object.
            float supportAreaRatio = 0;
            /// Whether the object is considered safe or unsafe.
            bool safe = true;
        };

        UD ud;
    };

    struct SupportEdge
    {
        /// Whether the separating plane is considered vertical.
        bool verticalSeparatingPlane = false;
        /// Whether the edge was added by uncertainty detection.
        bool fromUncertaintyDetection = false;
    };


    /**
     * @brief Representation of a binary support relation between scene objects.
     */
    class SupportGraph : public RelationGraph<SupportVertex, SupportEdge>
    {

    public:

        // Inherit contructors.
        using RelationGraph<SupportVertex, SupportEdge>::RelationGraph;

        std::string strVertex(ConstVertex v) const override;
        std::string strEdge(ConstEdge e) const override;


        /// Get a GraphvizWriter for visualization.
        static graphviz::Writer<SupportGraph> getGraphvizWriter();


    protected:

        /// Labels edges according to their attributes.
        static graphviz::Attributes graphvizEdgeFormat(ConstEdge edge);

    };

    extern template class graphviz::Writer<SupportGraph>;

}
