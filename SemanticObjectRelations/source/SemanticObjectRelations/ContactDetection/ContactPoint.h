#pragma once

#include <Eigen/Core>
#include <iosfwd>
#include <vector>

namespace semrel
{

class Shape;

/**
 * @brief Models one point of contact between two objects A and B.
 *
 * A contact point consists of the contact position between object A
 * and object B and the contact normal. The "normal on A" points from
 * A to B, and "the normal on B" points from B to A. Both normals are
 * identical except their sign (nA = -nB).
 */
class ContactPoint
{
public:
    /**
     * Construct from given parameters (with normal on B).
     * @param normal    The normal (on A or B, as specified by normalOnB).
     * @param normalOnB True if given normal is on B, false if it is on A.
     */
    ContactPoint(const Shape* shapeA = nullptr,
                 const Shape* shapeB = nullptr,
                 const Eigen::Vector3f& position = Eigen::Vector3f::Zero(),
                 const Eigen::Vector3f& normal = Eigen::Vector3f::Zero(),
                 bool normalOnB = true);


    /// Get object A.
    const Shape* getShapeA() const { return shapeA; }
    /// Set object A.
    void setShapeA(const Shape* shape) { shapeA = shape; }

    /// Get object B.
    const Shape* getShapeB() const { return shapeB; }
    /// Set object B.
    void setShapeB(const Shape* shape) { shapeB = shape; }


    /// Get the position of contact.
    Eigen::Vector3f getPosition() const { return position; }
    /// Set the position of contact.
    void setPosition(const Eigen::Vector3f& value) { position = value; }

    /// Get the normal on A.
    Eigen::Vector3f getNormalOnA() const { return -normalOnB; }
    /// Set the normal on A (getNormalOnB() will return `-value`).
    void setNormalOnA(const Eigen::Vector3f& value) { normalOnB = -value; }

    /// Get the normal on B.
    Eigen::Vector3f getNormalOnB() const { return normalOnB; }
    /// Set the normal on B (getNormalOnA() will return `-value`).
    void setNormalOnB(const Eigen::Vector3f& value) { normalOnB = value; }

private:

    /// The first object.
    const Shape* shapeA;
    /// The second object.
    const Shape* shapeB;
    /// The point of contact.
    Eigen::Vector3f position ;
    /// The normal on object B.
    Eigen::Vector3f normalOnB;

};

std::ostream& operator<<(std::ostream& os, const ContactPoint& cp);


/// Vector of ContactPoints.
using ContactPointList = std::vector<ContactPoint>;

}

