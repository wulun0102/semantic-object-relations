/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    corcal::core::ssr
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "evaluate_relations.h"

// STD/STL
#include <cmath>
#include <string>

#include <SemanticObjectRelations/Shapes.h>

#include "SpatialGraph.h"


using namespace semrel;

using Type = SpatialRelations::Type;


SpatialGraph spatial::evaluateRelations(const ShapeMap& currentObjects, const ShapeMap& pastObjects,
                                        const Parameters& parameters)
{
    SpatialGraph graph = initializeGraph(currentObjects, pastObjects);

    semrel::spatial::evaluateContactRelations(graph);
    semrel::spatial::evaluateStaticRelations(graph);
    semrel::spatial::evaluateDynamicRelations(graph, parameters);

    return graph;
}

SpatialGraph spatial::evaluateStaticRelations(const ShapeMap& objects)
{
    SpatialGraph graph = initializeGraph(objects, {});

    semrel::spatial::evaluateContactRelations(graph);
    semrel::spatial::evaluateStaticRelations(graph);

    return graph;
}


SpatialGraph spatial::initializeGraph(const ShapeMap& currentObjects, const ShapeMap& pastObjects)
{
    SpatialGraph graph(currentObjects);

    // Compute AABBs and make completely connected.
    for (auto u : graph.vertices())
    {
        // TODO: handle missing object.
        u.attrib().currentAABB = currentObjects.at(u.objectID())->getAABB();
        if (auto find = pastObjects.find(u.objectID()); find != pastObjects.end())
        {
            u.attrib().pastAABB = pastObjects.at(u.objectID())->getAABB();
        }
        else
        {
            u.attrib().pastAABB = AxisAlignedBoundingBox::Empty();
        }

        for (auto v : graph.vertices())
        {
            if (u != v)
            {
                graph.addEdge(u, v);
            }
        }
    }
    return graph;
}


void spatial::evaluateContactRelations(SpatialGraph& graph)
{
    for (auto edge : graph.edges())
    {
        if (edge.source().attrib().currentAABB.isColliding(edge.target().attrib().currentAABB))
        {
            edge.attrib().relations.set(Type::contact, true);
        }
    }
}


void spatial::evaluateStaticRelations(SpatialGraph& graph)
{
    // We rely on the fact that if "A right of B", then "B left of A".
    // => We only check for "left" to simultanously find "right".

    for (auto edge : graph.edges())
    {
        const AxisAlignedBoundingBox& source = edge.source().attrib().currentAABB;
        const AxisAlignedBoundingBox& target = edge.target().attrib().currentAABB;

        auto reverseEdge = graph.edge(edge.target(), edge.source());

        SpatialRelations& relations = edge.attrib().relations;
        SpatialRelations& reverseRelations = reverseEdge.attrib().relations;

        bool around = false;

        // Left / Right
        if (source.maxX() <= target.minX())
        {
            relations.set(Type::static_left_of, true);
            // <=>
            reverseRelations.set(Type::static_right_of, true);
            around = true;
        }

        // Below / Above
        if (source.maxZ() <= target.minZ())
        {
            relations.set(Type::static_below, true);
            // <=>
            reverseRelations.set(Type::static_above, true);
            around = true;
        }

        // Behind / In Front
        if (source.maxY() <= target.minY())
        {
            relations.set(Type::static_behind, true);
            // <=>
            reverseRelations.set(Type::static_in_front_of, true);
            around = true;
        }

        // Inside / Surround
        if (    target.minX() <= source.minX() and source.maxX() <= target.maxX()
            and target.minY() <= source.minY() and source.maxY() <= target.maxY()
            and target.minZ() <= source.maxZ() and source.maxZ() <= target.maxZ())
        {
            relations.set(Type::static_inside, true);
            // <=>
            reverseRelations.set(Type::static_surrounding, true);
        }


        // Around = Left or Right or Behind or In Front
        if (around)
        {
            relations.set(Type::static_around, true);
            // <=>
            reverseRelations.set(Type::static_around, true);
        }

        // Top / Bottom / contact around.
        for (SpatialRelations* rels : { &relations, &reverseRelations })
        {
            if (rels->get(Type::contact))
            {
                if (rels->get(Type::static_above))
                {
                    rels->set(Type::static_top, true);
                }
                if (rels->get(Type::static_below))
                {
                    rels->set(Type::static_bottom, true);
                }
                if (rels->get(Type::static_around))
                {
                    rels->set(Type::static_contact_around, true);
                }
            }
        }
    }
}


void spatial::evaluateDynamicRelations(SpatialGraph& graph, const Parameters& parameters)
{
    const float& distanceEqualityThreshold = parameters.distanceEqualityThreshold;

    for (auto edge : graph.edges())
    {
        const AxisAlignedBoundingBox& sourceCurrent = edge.source().attrib().currentAABB;
        const AxisAlignedBoundingBox& targetCurrent = edge.target().attrib().currentAABB;
        const AxisAlignedBoundingBox& sourcePast = edge.source().attrib().pastAABB;
        const AxisAlignedBoundingBox& targetPast = edge.target().attrib().pastAABB;

        if (sourcePast.empty() || targetPast.empty())
        {
            // Ignore uninitialized AABBs.
            continue;
        }

        auto reverseEdge = graph.edge(edge.target(), edge.source());

        const float delta_ab_current = aabb::centralDistance(sourceCurrent, targetCurrent);
        const float delta_ab_past = aabb::centralDistance(sourcePast, targetPast);

        // If the bounding boxes are colliding now and also did so earlier.
        const bool p1 = aabb::isColliding(sourceCurrent, targetCurrent)
                    and aabb::isColliding(sourcePast, targetPast);
        // If the bounding boxes are not colliding now and also did not earlier.
        const bool p2 = not aabb::isColliding(sourceCurrent, targetCurrent)
                    and not aabb::isColliding(sourcePast, targetPast);

        // If objects are in contact and were in contact earlier
        if (p1)
        {
            // Instead of checking if the centers of the objects are the very same point (as suggested in the
            // paper), we also compute delta and check for the distance to be below the distance eq. threshold zeta
            const bool p3 = aabb::centralDistance(sourceCurrent, sourcePast) > (distanceEqualityThreshold / 2);
            const bool p4 = aabb::centralDistance(targetCurrent, targetPast) > (distanceEqualityThreshold / 2);

            // If both objects moved
            if (p3 and p4)
            {
                edge.attrib().relations.set(Type::dynamic_moving_together, true);
                reverseEdge.attrib().relations.set(Type::dynamic_moving_together, true);
            }
            // If no object moved
            else if (!p3 and !p4)
            {
                edge.attrib().relations.set(Type::dynamic_halting_together, true);
                reverseEdge.attrib().relations.set(Type::dynamic_halting_together, true);
            }
            // If exactly one object moved
            else if (p3 xor p4)
            {
                edge.attrib().relations.set(Type::dynamic_fixed_moving_together, true);
                reverseEdge.attrib().relations.set(Type::dynamic_fixed_moving_together, true);
            }
        }
        // If objects were not in contact and were not in contact earlier
        else if (p2)
        {
            // If the distance now is (considerably) smaller than previously. Note: Added minus to the distance
            // equality threshold (zeta), which is not in the paper, but doesn't make sense otherwise
            if (delta_ab_current - delta_ab_past < -distanceEqualityThreshold)
            {
                edge.attrib().relations.set(Type::dynamic_getting_close, true);
                reverseEdge.attrib().relations.set(Type::dynamic_getting_close, true);
            }
            // If the distance now is (considerably) greater than previously
            else if (delta_ab_current - delta_ab_past > distanceEqualityThreshold)
            {
                edge.attrib().relations.set(Type::dynamic_moving_apart, true);
                reverseEdge.attrib().relations.set(Type::dynamic_moving_apart, true);
            }
            // This case is basically: | delta_ab - delta_ab_past | < zeta
            // In words: The distance between the objects now and then has not considerably changed
            else
            {
                edge.attrib().relations.set(Type::dynamic_stable, true);
                reverseEdge.attrib().relations.set(Type::dynamic_stable, true);
            }
        }
    }
}





