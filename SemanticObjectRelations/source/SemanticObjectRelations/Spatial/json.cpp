#include "json.h"

#include <SemanticObjectRelations/Shapes/json.h>
#include <SemanticObjectRelations/RelationGraph/json.h>


using namespace semrel;



void semrel::to_json(nlohmann::json& j, const SpatialRelations& rels)
{
    for (auto t : SpatialRelations::types)
    {
        j[SpatialRelations::names.at(t)] = rels.get(t);
    }
}

void semrel::from_json(const nlohmann::json& j, SpatialRelations& rels)
{
    for (auto t : SpatialRelations::types)
    {
        rels.set(t, j.at(SpatialRelations::names.at(t)).get<bool>());
    }
}

void semrel::to_json(nlohmann::json& j, const SpatialVertex& v)
{
    semrel::json::to_json_base(j, v);
    j["currentAABB"] = v.currentAABB;
    j["pastAABB"] = v.pastAABB;
}

void semrel::from_json(const nlohmann::json& j, SpatialVertex& v)
{
    semrel::json::from_json_base(j, v);
    v.currentAABB = j.at("currentAABB").get<AxisAlignedBoundingBox>();
    v.pastAABB = j.at("pastAABB").get<AxisAlignedBoundingBox>();
}


void semrel::to_json(nlohmann::json& j, const SpatialEdge& e)
{
    j["relations"] = e.relations;
}

void semrel::from_json(const nlohmann::json& j, SpatialEdge& e)
{
    e.relations = j.at("relations").get<SpatialRelations>();
}


