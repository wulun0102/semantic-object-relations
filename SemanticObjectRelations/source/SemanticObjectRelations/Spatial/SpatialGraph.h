#pragma once

#include <SemanticObjectRelations/RelationGraph.h>
#include <SemanticObjectRelations/Shapes/AxisAlignedBoundingBox.h>

#include "SpatialRelations.h"


namespace semrel
{

    struct SpatialVertex : public ShapeVertex
    {
        AxisAlignedBoundingBox currentAABB = AxisAlignedBoundingBox::Empty();
        AxisAlignedBoundingBox pastAABB = AxisAlignedBoundingBox::Empty();
    };

    struct SpatialEdge
    {
        SpatialRelations relations;
    };

    using SpatialGraphAttribs = NoAttrib;

    // using SpatialGraph = RelationGraph<SpatialVertex, SpatialEdge, SpatialGraphAttribs>;

    /// A graph encoding static and dynamic spatial relations.
    class SpatialGraph : public RelationGraph<SpatialVertex, SpatialEdge, SpatialGraphAttribs>
    {
    public:

        using RelationGraph::RelationGraph;


        /**
         * @brief Return copy of `*this` only containing edges whose relations
         * have a relation in `relationMask`.
         *
         * The edge relations themselves are not changed.
         */
        SpatialGraph filtered(const SpatialRelations& relationMask) const;


    };


}
