#pragma once

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>


class ParameterParsingError;


class ParameterSet
{

public:
    
    explicit ParameterSet(const std::string& name ="");
    virtual ~ParameterSet();
    
    virtual void setParameter(const std::string& name, const std::string& value) = 0;
    
    
    std::string getName() const;
    void setName(const std::string& value);
    
    
protected:
    
    static void checkFileOpened(const std::ifstream& ifs, const std::string& filename);
    
    static bool isIgnoredLine(const std::string& line);
    static std::pair<std::string, std::string> parseAssignmentLine(const std::string& line);
    
    static bool parseBool(const std::string& boolString);
    static int parseInt(const std::string& intString);
    static float parseFloat(const std::string& floatString);
    
    
    static void ltrim(std::string& s);
    static void rtrim(std::string& s);
    static void trim(std::string& s);
    
    static ParameterParsingError unknownParameterError(const std::string& paramName);
    
    
private:
    
    std::string name;
    
};


class ParameterParsingError : public std::runtime_error
{
public:
    
    ParameterParsingError(const std::string& msg);
    ParameterParsingError(const std::string& msg, int lineNr, const std::string& line);
    
    virtual const char* what() const noexcept override;
    
    std::string getMsg() const;
    void setMsg(const std::string& value);
    int getLineNr() const;
    void setLineNr(int value);
    std::string getLine() const;
    void setLine(const std::string& value);
    
private:
    
    void updateMessage();
    
    std::string msg = "";
    int lineNr = -1;
    std::string line = "";
    std::string fullMsg = ""; ///< constructed from lineNr and msg. Always up-to-date.
};
