macro(BulletCheckPrecision)
    message(STATUS "Testing Bullet for use of double precision...")
    try_compile( _result ${PROJECT_BINARY_DIR}
           ${PROJECT_SOURCE_DIR}/../cmake/BulletCheckPrecision.cpp
           CMAKE_FLAGS
               "-DINCLUDE_DIRECTORIES:string=${BULLET_INCLUDE_DIRS}"
               "-DLINK_LIBRARIES:string=${BULLET_LIBRARIES}"
               "-DCMAKE_CXX_FLAGS:string=-std=c++11"
           COMPILE_DEFINITIONS
               "-DBT_USE_DOUBLE_PRECISION"
           OUTPUT_VARIABLE _buildOut
    )
   if( _result )
       message(STATUS "Bullet double precision detected. Automatically defining BT_USE_DOUBLE_PRECISION")
       set(BULLET_USE_SINGLE_PRECISION OFF CACHE BOOL "" FORCE)
       add_definitions(-DBT_USE_DOUBLE_PRECISION)
   else()
       # Try it *without* -DBT_USE_DOUBLE_PRECISION to make sure it's single...
       set( _result )
       set( _buildOut )
       try_compile( _result ${PROJECT_BINARY_DIR}
           ${PROJECT_SOURCE_DIR}/../cmake/BulletCheckPrecision.cpp
           CMAKE_FLAGS
               "-DINCLUDE_DIRECTORIES:string=${BULLET_INCLUDE_DIRS}"
               "-DLINK_LIBRARIES:string=${BULLET_LIBRARIES}"
           OUTPUT_VARIABLE _buildOut
       )
       if( _result )
           message(STATUS "Bullet single precision detected. Not defining BT_USE_DOUBLE_PRECISION")
           set(BULLET_USE_SINGLE_PRECISION ON CACHE BOOL "" FORCE)
       else()
           message(ERROR "Unable to determine single or double precision.")
           message("Build output follows:")
           message("${_buildOut}")
       endif()
   endif()
endmacro()
