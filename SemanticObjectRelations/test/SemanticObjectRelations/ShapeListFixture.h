#pragma once

#include <boost/test/test_tools.hpp>

#include <SemanticObjectRelations/Shapes.h>


namespace Eigen
{
    bool operator==(const Quaternionf& lhs, const Quaternionf& rhs);
    std::ostream& operator<<(std::ostream& os, const Quaternionf& rhs);
}


namespace semrel::test
{
    template <class ShapeT>
    ShapeT makeShape(long id)
    {
        return ShapeT(ShapeID(id));
    }

    template <> Box makeShape<Box>(long id);
    template <> Cylinder makeShape<Cylinder>(long id);
    template <> Sphere makeShape<Sphere>(long id);


    template <class ShapeT>
    std::unique_ptr<ShapeT> makeShapePtr(long id)
    {
        return std::make_unique<ShapeT>(makeShape<ShapeT>(id));
    }

    ShapeList makeShapeList(std::size_t num, long maxID = 100);


    struct ShapeListFixture
    {
        ShapeList shapeList;

        ShapeListFixture(std::size_t numShapes = 5);

        ShapeID getID(const std::size_t i) const;
        std::vector<ShapeID> getIdList() const;
    };



    void check_equal_base(const Shape& lhs, const Shape& rhs);
    void check_equal_derived(const Box& lhs, const Box& rhs);
    void check_equal_derived(const Cylinder& lhs, const Cylinder& rhs);
    void check_equal_derived(const Sphere& lhs, const Sphere& rhs);


    template <class ShapeT>
    void check_equal(const ShapeT& lhs, const ShapeT& rhs)
    {
        check_equal_base(lhs, rhs);
        check_equal_derived(lhs, rhs);
    }

    template <class ShapeT>
    void check_equal(const ShapePtr& lhs, const ShapePtr& rhs)
    {
        BOOST_CHECK(lhs);
        if (lhs)
        {
            check_equal_base(*lhs, *rhs);
        }

        const ShapeT* cast = dynamic_cast<const ShapeT*>(lhs.get());
        BOOST_CHECK(cast);

        if (cast)
        {
            check_equal_derived(*cast, *dynamic_cast<const ShapeT*>(rhs.get()));
        }
    }

}
