#include "ShapeListFixture.h"

#include <random>
#include <set>

#include <boost/test/test_tools.hpp>


bool Eigen::operator==(const Eigen::Quaternionf& lhs, const Eigen::Quaternionf& rhs)
{
    return lhs.isApprox(rhs);
}

std::ostream& Eigen::operator<<(std::ostream& os, const Quaternionf& rhs)
{
    return os << "(" << rhs.w() << " | " << rhs.x() << " " << rhs.y() << " " << rhs.z() << ")";
}


namespace semrel
{

    test::ShapeListFixture::ShapeListFixture(std::size_t numShapes) :
        shapeList(test::makeShapeList(numShapes))
    {}

    ShapeID test::ShapeListFixture::getID(const std::size_t i) const
    {
        return shapeList.at(i)->getID();
    }

    std::vector<ShapeID> test::ShapeListFixture::getIdList() const
    {
        std::vector<ShapeID> ids;
        for (const auto& shape : shapeList)
        {
            ids.push_back(shape->getID());
        }
        return ids;
    }


    ShapeList test::makeShapeList(std::size_t num, long maxID)
    {
        std::default_random_engine eng(std::random_device{}());
        std::uniform_int_distribution<long> distID(0, maxID);
        std::uniform_int_distribution<int> distType(0, 3);

        std::set<ShapeID> ids;
        while (ids.size() < num)
        {
            ids.emplace(distID(eng));
        }

        ShapeList objects;
        for (auto id : ids)
        {
            int type = distType(eng);
            switch (type)
            {
            case 0:
                objects.emplace_back(makeShapePtr<Box>(id));
                break;
            case 1:
                objects.emplace_back(makeShapePtr<Cylinder>(id));
                break;
            case 2:
                objects.emplace_back(makeShapePtr<Sphere>(id));
                break;
            case 3:
                objects.emplace_back(makeShapePtr<MeshShape>(id));
                break;
            }
        }

        BOOST_REQUIRE_EQUAL(objects.size(), num);

        return objects;
    }

    template <>
    Box test::makeShape<Box>(long id)
    {
        return Box(Eigen::Vector3f(1, 2, -3),
                   Eigen::Quaternionf(Eigen::AngleAxisf(1, Eigen::Vector3f::UnitX())),
                   Eigen::Vector3f(3, 4, 5),
                   ShapeID(id));
    }

    template <>
    Cylinder test::makeShape<Cylinder>(long id)
    {
        return Cylinder(Eigen::Vector3f(1, 2, -3), Eigen::Vector3f(1, 1, -1).normalized(),
                        2.5, 5.5, ShapeID(id));
    }

    template <>
    Sphere test::makeShape<Sphere>(long id)
    {
        return Sphere(Eigen::Vector3f(1, 2, -3), 3.5, ShapeID(id));
    }


    void test::check_equal_base(const Shape& lhs, const Shape& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.getID(), rhs.getID());
        BOOST_CHECK_EQUAL(lhs.getPosition(), rhs.getPosition());
        BOOST_CHECK_EQUAL(lhs.getOrientation(), rhs.getOrientation());
    }

    void test::check_equal_derived(const Box& lhs, const Box& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.getExtents(), rhs.getExtents());
    }

    void test::check_equal_derived(const Cylinder& lhs, const Cylinder& rhs)
    {
        BOOST_CHECK_CLOSE(lhs.getRadius(), rhs.getRadius(), 1e-6f);
        BOOST_CHECK_CLOSE(lhs.getHeight(), rhs.getHeight(), 1e-6f);
    }

    void test::check_equal_derived(const Sphere& lhs, const Sphere& rhs)
    {
        BOOST_CHECK_CLOSE(lhs.getRadius(), rhs.getRadius(), 1e-6f);
    }





}


