
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>
#include <SemanticObjectRelations/Shapes.h>

#include "../ShapeListFixture.h"


using namespace semrel;


struct VA : public ShapeVertex
{
    int vertexData;
};
struct EA
{
    int edgeData;
};
struct GA
{
    int graphData;
};


namespace
{
struct GraphConstructionFixture : public test::ShapeListFixture
{
    GA graphAttribute {42};

    GraphConstructionFixture()
    {
    }

    template <class VertexT>
    void checkVertex(const VertexT& v, const Shape* shape)
    {
        BOOST_CHECK_EQUAL(v.objectID(), shape->getID());
    }

    template <class VertexT>
    void checkVertex(const VertexT& v, const ShapePtr& shape)
    {
        checkVertex(v, shape.get());
    }


    template <class GraphT>
    void checkConstructedGraph(const GraphT& graph, const ShapeList& shapeList)
    {
        BOOST_CHECK_EQUAL(graph.numVertices(), shapeList.size());
        BOOST_CHECK_EQUAL(graph.numEdges(), 0);

        for (const auto& shape : shapeList)
        {
            auto v = graph.vertex(*shape);
            checkVertex(v, shape);
        }
    }
    template <class GraphT, class ShapePtrT>
    void checkConstructedGraph(const GraphT& graph, const std::map<ShapeID, ShapePtrT>& shapeMap)
    {
        BOOST_CHECK_EQUAL(graph.numVertices(), shapeMap.size());
        BOOST_CHECK_EQUAL(graph.numEdges(), 0);

        for (const auto& [id, shape] : shapeMap)
        {
            auto v = graph.vertex(id);
            checkVertex(v, shape);
        }
    }
};
}


BOOST_FIXTURE_TEST_SUITE(RelationGraphConstructorTests, GraphConstructionFixture)


BOOST_AUTO_TEST_CASE(test_default_constructor)
{
    RelationGraph<> graph1;
    RelationGraph<ShapeVertex, NoAttrib, GA> graph2(graphAttribute);

    checkConstructedGraph(graph1, {});
    checkConstructedGraph(graph2, {});
}

BOOST_AUTO_TEST_CASE(test_constructor_from_vector_of_ShapeID)
{
    const std::vector<ShapeID> ids = getIdList();

    RelationGraph<> graph1(ids);
    RelationGraph<ShapeVertex, NoAttrib, GA> graph2(ids, graphAttribute);

    checkConstructedGraph(graph1, shapeList);
    checkConstructedGraph(graph2, shapeList);
}

BOOST_AUTO_TEST_CASE(test_constructor_from_ShapeList)
{
    RelationGraph<> graph1(shapeList);
    RelationGraph<ShapeVertex, NoAttrib, GA> graph2(shapeList, graphAttribute);

    checkConstructedGraph(graph1, shapeList);
    checkConstructedGraph(graph2, shapeList);
}

BOOST_AUTO_TEST_CASE(test_constructor_from_ShapeMap)
{
    const ShapeMap shapeMap = toShapeMap(shapeList);

    RelationGraph<> graph1(shapeMap);
    RelationGraph<ShapeVertex, NoAttrib, GA> graph2(shapeMap, graphAttribute);

    checkConstructedGraph(graph1, shapeMap);
    checkConstructedGraph(graph2, shapeMap);
}

BOOST_AUTO_TEST_SUITE_END()


struct GraphFixture : public test::ShapeListFixture
{
    GraphFixture(size_t numShapes = 5) : test::ShapeListFixture(numShapes)
    {
    }

    template <typename GraphT>
    void addEdge(GraphT& graph, std::size_t u, std::size_t v)
    {
        graph.addEdge(getID(u), getID(v));
    }
};

#define CHECK_EDGE(graph, v1, v2) \
    BOOST_CHECK(graph.hasEdge(v1, v2)); \
    BOOST_CHECK_NO_THROW(graph.edge(v1, v2))

#define CHECK_NO_EDGE(graph, v1, v2) \
    BOOST_CHECK(!graph.hasEdge(v1, v2)); \
    BOOST_CHECK_THROW(graph.edge(v1, v2), error::NoEdgeBetween)


BOOST_FIXTURE_TEST_SUITE(RelationGraphTest, GraphFixture)


BOOST_AUTO_TEST_CASE(test_edge_operations)
{
    using Graph = RelationGraph<VA>;
    using Vertex = Graph::Vertex;
    using Edge = Graph::Edge;

    Graph graph(shapeList);

    Vertex v0 = graph.vertex(0);
    Vertex v1 = graph.vertex(1);
    Vertex v2 = graph.vertex(2);

    // No edges at beginning.
    CHECK_NO_EDGE(graph, v1, v2);
    CHECK_NO_EDGE(graph, v2, v1);
    BOOST_CHECK_EQUAL(graph.numEdges(), 0);

    // Add 1st edge (v1->v2).
    graph.addEdge(v1, v2);

    CHECK_EDGE(graph, v1, v2);
    CHECK_NO_EDGE(graph, v2, v1);
    BOOST_CHECK_EQUAL(graph.numEdges(), 1);

    // Adding existing edge is ignored.
    graph.addEdge(v1, v2);
    BOOST_CHECK_EQUAL(graph.numEdges(), 1);
    // Same when adding by ID.
    graph.addEdge(v1.objectID(), v2.objectID());
    BOOST_CHECK_EQUAL(graph.numEdges(), 1);

    // Add second edge by ID (v2->v1).
    graph.addEdge(v2.objectID(), v1.objectID());
    BOOST_CHECK_EQUAL(graph.numEdges(), 2);

    // Adding existing edge is ignored.
    graph.addEdge(v2, v1);
    BOOST_CHECK_EQUAL(graph.numEdges(), 2);


    // Check vertices of edges.
    Edge e = graph.edge(v1, v2);
    BOOST_CHECK_EQUAL(e.source(), v1);
    BOOST_CHECK_EQUAL(e.target(), v2);

    BOOST_CHECK_EQUAL(graph.edge(v2, v1).source(), v2);
    BOOST_CHECK_EQUAL(graph.edge(v2, v1).target(), v1);


    // Removing non-existing edge fails.
    CHECK_NO_EDGE(graph, v0, v1);
    BOOST_CHECK_THROW(graph.removeEdge(graph.edge(v0, v1)), error::NoEdgeBetween);
    BOOST_CHECK_EQUAL(graph.numEdges(), 2);

    // Removing existing edge succeeds.
    BOOST_CHECK_NO_THROW(graph.removeEdge(graph.edge(v1, v2)));
    BOOST_CHECK_EQUAL(graph.numEdges(), 1);
    CHECK_NO_EDGE(graph, v1, v2);
    CHECK_EDGE(graph, v2, v1);
}


BOOST_AUTO_TEST_CASE(test_graph_attribs)
{
    using Graph = RelationGraph<ShapeVertex, EA, GA>;

    Graph g1;

    g1.attrib().graphData = 42;
    BOOST_CHECK_EQUAL(g1.attrib().graphData, 42);

    Graph g2 = g1;
    BOOST_CHECK_EQUAL(g2.attrib().graphData, 42);
}


BOOST_AUTO_TEST_CASE(test_vertex_attribs)
{
    using Graph = RelationGraph<VA>;
    using Vertex = Graph::Vertex;

    Graph graph(shapeList);

    int data = 0;
    for (Vertex v : graph.vertices())
    {
        v.attrib().vertexData = data;
        data++;
    }

    data = 0;
    for (Vertex v : graph.vertices())
    {
        BOOST_CHECK_EQUAL(v.attrib().vertexData, data);
        data++;
    }

    for (std::size_t i = 0; i < graph.numVertices(); ++i)
    {
        BOOST_CHECK_EQUAL(graph.vertex(i).attrib().vertexData, i);
    }
}



BOOST_AUTO_TEST_CASE(test_vertex_attribs_access)
{
    using Graph = RelationGraph<ShapeVertex>;

    shapeList.resize(2);

    Shape* shp1 = shapeList[0].get();
    Shape* shp2 = shapeList[1].get();

    Graph graph(shapeList);

    Graph::Vertex v1 = graph.vertex(shp1->getID());
    Graph::Vertex v2 = graph.vertex(shp2->getID());
    ShapeID id1 = shp1->getID();
    ShapeID id2 = shp2->getID();

    graph.addEdge(v1, v2);
    graph.addEdge(id2, id1);
    BOOST_CHECK_EQUAL(graph.numEdges(), 2);

    // Check object ID.
    BOOST_CHECK_EQUAL(v1.objectID(), shp1->getID());
    BOOST_CHECK_EQUAL(v2.objectID(), shp2->getID());

    // Check object ID.
    BOOST_CHECK_EQUAL(graph.vertex(id1).objectID(), shp1->getID());
    BOOST_CHECK_EQUAL(graph.vertex(id2).objectID(), shp2->getID());

    // Check object ID.
    BOOST_CHECK_EQUAL(graph.vertex(shp1->getID()).objectID(), shp1->getID());
    BOOST_CHECK_EQUAL(graph.vertex(shp2->getID()).objectID(), shp2->getID());


    // Check vertex-based attrib.
    BOOST_CHECK_EQUAL(v1.attrib().objectID, shp1->getID());
    BOOST_CHECK_EQUAL(v2.attrib().objectID, shp2->getID());

    // Check id-based attrib.
    BOOST_CHECK_EQUAL(graph.vertex(id1).attrib().objectID, shp1->getID());
    BOOST_CHECK_EQUAL(graph.vertex(id2).attrib().objectID, shp2->getID());

    // Check id-based attrib without explicit variable storage.
    BOOST_CHECK_EQUAL(graph.vertex(shp1->getID()).attrib().objectID, shp1->getID());
    BOOST_CHECK_EQUAL(graph.vertex(shp2->getID()).attrib().objectID, shp2->getID());

    // Check vertex-based operator [].
    BOOST_CHECK_EQUAL(graph[v1].objectID, shp1->getID());
    BOOST_CHECK_EQUAL(graph[v2].objectID, shp2->getID());

    // Check id-based operator [].
    BOOST_CHECK_EQUAL(graph[shp1->getID()].objectID, shp1->getID());
    BOOST_CHECK_EQUAL(graph[shp2->getID()].objectID, shp2->getID());

}

BOOST_AUTO_TEST_CASE(test_edge_attribs)
{
    using Graph = RelationGraph<ShapeVertex, EA>;

    shapeList.resize(2);

    Shape* shp1 = shapeList[0].get();
    Shape* shp2 = shapeList[1].get();

    Graph graph(shapeList);

    Graph::Vertex v1 = graph.vertex(shp1->getID());
    Graph::Vertex v2 = graph.vertex(shp2->getID());
    ShapeID id1 = shp1->getID();
    ShapeID id2 = shp2->getID();

    graph.addEdge(v1, v2);
    graph.edge(v1, v2).attrib().edgeData = 12;

    EA ea21 {21};
    graph.addEdge(id2, id1, ea21);

    BOOST_CHECK_EQUAL(graph.numEdges(), 2);

    BOOST_CHECK_EQUAL(graph.edge(v1, v2).attrib().edgeData, 12);
    BOOST_CHECK_EQUAL(graph.edge(v2, v1).attrib().edgeData, 21);
}


BOOST_AUTO_TEST_CASE(test_hasPath)
{
    using Graph = RelationGraph<ShapeVertex, EA>;

    /* Graph: (vertices 0 - 4)
     *
     * 0 -> 1 -> 2 -> 3
     *      +--> 4 -->|
     */

    Graph graph(shapeList);

    addEdge(graph, 0, 1);
    addEdge(graph, 1, 2);
    addEdge(graph, 1, 4);
    addEdge(graph, 2, 3);
    addEdge(graph, 4, 3);

    // direct path 0 -> 1
    BOOST_CHECK( graph.hasPath(getID(0), getID(1)));
    BOOST_CHECK(!graph.hasPath(getID(1), getID(0)));

    // distant path 0 -> ... -> 3
    BOOST_CHECK( graph.hasPath(getID(0), getID(3)));
    BOOST_CHECK(!graph.hasPath(getID(3), getID(0)));

    // 2 -> 3, but not 2 -> 4
    BOOST_CHECK( graph.hasPath(getID(2), getID(3)));
    BOOST_CHECK(!graph.hasPath(getID(2), getID(4)));

    // 4 -> 3, but not 4 -> 2
    BOOST_CHECK( graph.hasPath(getID(4), getID(3)));
    BOOST_CHECK(!graph.hasPath(getID(4), getID(2)));
}


BOOST_AUTO_TEST_CASE(test_findPath)
{
    using Graph = RelationGraph<ShapeVertex, EA>;

    /* Graph: (vertices 0 - 4)
     *
     * 0 -> 1 -> 2 -> 3
     * +--> 4 ------->|
     */

    Graph graph(shapeList);

    addEdge(graph, 0, 1);
    addEdge(graph, 1, 2);
    addEdge(graph, 2, 3);
    addEdge(graph, 0, 4);
    addEdge(graph, 4, 3);


    std::vector<Graph::ConstVertex> path;
    std::vector<Graph::ConstVertex> correct;

    // 0 -> 1
    path = graph.findPath(getID(0), getID(1));
    correct = { graph.vertex(getID(0)), graph.vertex(getID(1)) };

    BOOST_CHECK_EQUAL_COLLECTIONS(path.begin(), path.end(),
                                  correct.begin(), correct.end());

    // 0 -> 1 -> 2
    path = graph.findPath(getID(0), getID(2));
    correct = { graph.vertex(getID(0)), graph.vertex(getID(1)), graph.vertex(getID(2)) };

    BOOST_CHECK_EQUAL_COLLECTIONS(path.begin(), path.end(),
                                  correct.begin(), correct.end());

    // 0 -> 4 -> 3  (shorter than 0 -> 1 -> 2 -> 3)
    path = graph.findPath(getID(0), getID(3));
    correct = { graph.vertex(getID(0)), graph.vertex(getID(4)), graph.vertex(getID(3)) };

    BOOST_CHECK_EQUAL_COLLECTIONS(path.begin(), path.end(),
                                  correct.begin(), correct.end());

    // no paths
    BOOST_CHECK(graph.findPath(getID(1), getID(0)).empty());
    BOOST_CHECK(graph.findPath(getID(2), getID(0)).empty());
    BOOST_CHECK(graph.findPath(getID(2), getID(1)).empty());
}

BOOST_AUTO_TEST_SUITE_END()


namespace
{
struct GraphOutputFixture : public GraphFixture
{
    using Graph = RelationGraph<VA, EA, GA>;

    Graph graph { shapeList };

    GraphOutputFixture() : GraphFixture (5)
    {
        addEdge(graph, 0, 1);
        addEdge(graph, 1, 2);
        addEdge(graph, 2, 1);
        addEdge(graph, 3, 3);
    }
};
}


BOOST_FIXTURE_TEST_SUITE(RelationGraph_StringOutputTest, GraphOutputFixture)


BOOST_AUTO_TEST_CASE(test_strVertex)
{
    for (auto v : graph.vertices())
    {
        const std::string str = graph.strVertex(v);
        BOOST_CHECK(!str.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_strEdge)
{
    for (auto e : graph.edges())
    {
        const std::string str = graph.strEdge(e);
        BOOST_CHECK(!str.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_str)
{
    const std::string str = graph.str();
    BOOST_CHECK(!str.empty());
}

BOOST_AUTO_TEST_CASE(test_stream_operator)
{
    std::stringstream ss;
    ss << graph;
    BOOST_CHECK_EQUAL(ss.str(), graph.str());
}


BOOST_AUTO_TEST_SUITE_END()
