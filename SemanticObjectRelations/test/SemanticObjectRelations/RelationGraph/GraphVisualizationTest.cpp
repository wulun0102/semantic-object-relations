
#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/graph/graphviz.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>


#include <SemanticObjectRelations/RelationGraph/graphviz.h>
#include <SemanticObjectRelations/RelationGraph/graphviz/WriterImpl.h>
#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>

#include "../ShapeListFixture.h"


using namespace semrel;
namespace fs = std::filesystem;

struct VertexAttrib : public ShapeVertex
{
    std::string name;
};
struct EdgeAttrib
{
    std::size_t weight;
};

using Graph = RelationGraph<VertexAttrib, EdgeAttrib>;


void checkFileExists(const std::string& filename)
{
    BOOST_CHECK_MESSAGE(fs::is_regular_file(filename),
                        "File '" << filename << "' should exist, but does not.");
}
void checkFileExistsAndNotEmpty(const std::string& filename)
{
    BOOST_CHECK_MESSAGE(fs::is_regular_file(filename),
                        "File '" << filename << "' should exist, but does not.");
    std::ifstream ifs(filename);
    const bool empty = ifs.peek() == std::ifstream::traits_type::eof();
    BOOST_CHECK_MESSAGE(!empty,
                        "File '" << filename << "' should not be empty, but is empty.");
}

void checkFileNotExists(const std::string& filename)
{
    BOOST_CHECK_MESSAGE(!fs::is_regular_file(filename),
                        "File '" << filename << "' should not exist, but does.");
}

static const std::string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWQXYZ";

static const std::string FILENAME_BASE = "test_graph";
static const std::string FILENAME_DOT = "test_graph.dot";
static const std::string FILENAME_IMG = "test_graph.png";

static const bool CLEANUP = true;


struct Fixture
{
    ShapeList objects = test::makeShapeList(10);
    Graph graph;

    Fixture()
    {
        graph = Graph(objects);

        for (std::size_t i = 0; i < graph.numVertices(); ++i)
        {
            graph.vertex(i).attrib().name = ALPHABET[i % ALPHABET.size()];
        }

        for (std::size_t i = 0; i < 8; ++i)
        {
            graph.addEdge(graph.vertex(i), graph.vertex(i + 2), EdgeAttrib { i * (i + 2) });
        }

        if (CLEANUP)
        {
            // Clean up expected files.
            cleanup();
        }
    }

    ~Fixture()
    {
        cleanup();
    }

    void cleanup()
    {
        fs::remove(FILENAME_DOT);
        fs::remove(FILENAME_IMG);
        checkFileNotExists(FILENAME_DOT);
        checkFileNotExists(FILENAME_IMG);
    }
};


struct FixtureObjectAgnostic : public Fixture
{
    graphviz::Writer<Graph> writer;

    FixtureObjectAgnostic()
    {
        auto vertexFormat = [](Graph::ConstVertex v)
        {
            return graphviz::Attributes{{ "label", v.attrib().name }};
        };
        auto edgeFormat = [](Graph::ConstEdge e)
        {
            return graphviz::Attributes{{ "label", std::to_string(e.attrib().weight) }};
        };

        writer = graphviz::Writer<Graph>()
                .setVertexFormat(vertexFormat)
                .setEdgeFormat(edgeFormat);
    }
};



BOOST_FIXTURE_TEST_SUITE(ObjectAgnosticGraphVisualizationTest, FixtureObjectAgnostic)

BOOST_AUTO_TEST_CASE(test_write_mode_default_object_agnostic)
{
    // Default behaviour: generate both
    BOOST_CHECK(writer.write(graph, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}


BOOST_AUTO_TEST_CASE(test_write_mode_DOT_PNG_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::DOT_PNG);
    BOOST_CHECK(writer.write(graph, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}

BOOST_AUTO_TEST_CASE(test_write_mode_PNG_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::PNG);
    BOOST_CHECK(writer.write(graph, FILENAME_BASE));
    checkFileNotExists(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}

BOOST_AUTO_TEST_CASE(test_write_mode_DOT_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::DOT);
    BOOST_CHECK(writer.write(graph, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileNotExists(FILENAME_IMG);
}

BOOST_AUTO_TEST_SUITE_END()


struct FixtureObjectAware : public Fixture
{
    ShapeMap objectMap;
    graphviz::Writer<Graph> writer;

    FixtureObjectAware()
    {
        objectMap = toShapeMap(objects);

        auto vertexFormat = [](Graph::ConstVertex v, const Shape* object)
        {
            return graphviz::Attributes{{ "label", v.attrib().name + " " + object->tag() }};
        };
        auto edgeFormat = [](Graph::ConstEdge e, const Shape* sourceObject, const Shape* targetObject)
        {
            return graphviz::Attributes{{ "label",
                    sourceObject->tag() + " -> " + targetObject->tag() + ": " + std::to_string(e.attrib().weight)
                }};
        };

        writer = graphviz::Writer<Graph>()
                .setVertexShapeFormat(vertexFormat)
                .setEdgeShapeFormat(edgeFormat);
    }
};


BOOST_FIXTURE_TEST_SUITE(ObjectAwareGraphVisualizationTest, FixtureObjectAware)

BOOST_AUTO_TEST_CASE(test_write_mode_default_object_agnostic)
{
    // Default behaviour: generate both
    BOOST_CHECK(writer.write(graph, objectMap, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}

BOOST_AUTO_TEST_CASE(test_write_mode_DOT_PNG_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::DOT_PNG);
    BOOST_CHECK(writer.write(graph, objectMap, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}

BOOST_AUTO_TEST_CASE(test_write_mode_PNG_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::PNG);
    BOOST_CHECK(writer.write(graph, objectMap, FILENAME_BASE));
    checkFileNotExists(FILENAME_DOT);
    checkFileExistsAndNotEmpty(FILENAME_IMG);
}

BOOST_AUTO_TEST_CASE(test_write_mode_DOT_object_agnostic)
{
    writer.setMode(graphviz::WriterMode::DOT);
    BOOST_CHECK(writer.write(graph, objectMap, FILENAME_BASE));
    checkFileExistsAndNotEmpty(FILENAME_DOT);
    checkFileNotExists(FILENAME_IMG);
}

BOOST_AUTO_TEST_SUITE_END()
