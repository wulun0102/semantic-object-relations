
#include <SemanticObjectRelations/Shapes/utils/LineIntersection.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>


using namespace semrel;
using namespace Eigen;


static IOFormat iof = IOFormat(4, 0, " ", "", "", "", "[", "]");


#define LOG_T_P \
    << "\n\tt = " << result.parameter << ", " \
    << "P = " << result.point.format(iof)


BOOST_AUTO_TEST_SUITE(LineIntersectionTest)


BOOST_AUTO_TEST_CASE(TestLineIntersectionTriangle)
{
    // build line
    const Vector3f origin(0, 1, 1);
    const Vector3f direction(0, 0, 1);
    const ParametrizedLine3f line(origin, direction);


    // build TriMesh
    MeshShape shape;
    TriMesh& mesh = shape.mesh();

    mesh.addVertex(Vector3f(1, 0, 0));
    mesh.addVertex(Vector3f(0, 2, 0));
    mesh.addVertex(Vector3f(-1, 0, 0));

    mesh.addVertex(Vector3f(1, 0, 3));
    mesh.addVertex(Vector3f(0, 2, 3));
    mesh.addVertex(Vector3f(-1, 0, 3));

    // add a triangle behind origin
    mesh.addTriangle(TriMesh::Triangle(0, 1, 2));

    LineIntersectionResult result = semrel::intersectWithLine(shape, line, true);

    BOOST_CHECK_MESSAGE(!result.found, "Hit a triangle behind ray although asRay=true. " LOG_T_P);

    result = semrel::intersectWithLine(shape, line, false);
    BOOST_CHECK_MESSAGE(result.found, "Missed a triangle behind ray although asRay=false." LOG_T_P);

    // add a triangle in front of
    mesh.addTriangle(TriMesh::Triangle(3, 4, 5));

    result = semrel::intersectWithLine(shape, line, true);
    BOOST_CHECK_MESSAGE(result.found, "Did not intersect a triangle in front of ray." LOG_T_P);
    BOOST_CHECK_GT(result.parameter, 0);

    result = semrel::intersectWithLine(shape, line, false);
    BOOST_CHECK_MESSAGE(result.found, "Did not intersect a triangle in front of ray." LOG_T_P);
    BOOST_CHECK_LT(result.parameter, 0);
}

BOOST_AUTO_TEST_SUITE_END()
