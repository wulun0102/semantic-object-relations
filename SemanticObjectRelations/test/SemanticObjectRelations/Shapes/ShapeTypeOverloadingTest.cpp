#include <SemanticObjectRelations/Shapes.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

using namespace semrel;

class Overloader
{
public:
    Overloader() {}

    int overloaded(const Shape& shape)
    {
        if (auto box = dynamic_cast<const Box*>(&shape))
        {
            return overloaded(*box);
        }
        else if (auto cyl = dynamic_cast<const Cylinder*>(&shape))
        {
            return overloaded(*cyl);
        }
        else if (auto sph = dynamic_cast<const Sphere*>(&shape))
        {
            return overloaded(*sph);
        }
        else if (auto msh = dynamic_cast<const MeshShape*>(&shape))
        {
            return overloaded(*msh);
        }
        else
        {
            // should not be reached
            std::stringstream msg;
            msg << "Received shape of unexpected type: \n" << shape.str();
            throw std::invalid_argument(msg.str());
        }
    }

    int overloaded(const Box& box) { (void) box; return 1; }
    int overloaded(const Cylinder& cyl) { (void) cyl; return 2; }
    int overloaded(const Sphere& sph) { (void) sph; return 3; }
    int overloaded(const MeshShape& msh) { (void) msh; return 4; }

};


BOOST_AUTO_TEST_SUITE(ShapeTypeOverloadingTest)

BOOST_AUTO_TEST_CASE(TestShapeTypeOverloading)
{
    Box box;
    Cylinder cyl;
    Sphere sph;
    MeshShape msh;

    Overloader ov;

    BOOST_CHECK_EQUAL(ov.overloaded(box), 1);
    BOOST_CHECK_EQUAL(ov.overloaded(cyl), 2);
    BOOST_CHECK_EQUAL(ov.overloaded(sph), 3);
    BOOST_CHECK_EQUAL(ov.overloaded(msh), 4);

    Shape* shape;

    shape = &box;
    BOOST_CHECK_EQUAL(ov.overloaded(*shape), 1);
    shape = &cyl;
    BOOST_CHECK_EQUAL(ov.overloaded(*shape), 2);
    shape = &sph;
    BOOST_CHECK_EQUAL(ov.overloaded(*shape), 3);
    shape = &msh;
    BOOST_CHECK_EQUAL(ov.overloaded(*shape), 4);
}

BOOST_AUTO_TEST_SUITE_END()
