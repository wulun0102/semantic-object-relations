
#include <SemanticObjectRelations/Shapes/json.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../../ShapeListFixture.h"

#include "tools.h"
#include "RegisteredShape.h"


using namespace semrel;
using namespace semrel::test;


BOOST_AUTO_TEST_SUITE(Shape_Json_Custom)


BOOST_AUTO_TEST_CASE(test_to_json_Unregistered_throws_exception)
{
    std::unique_ptr<Shape> shapeIn = std::make_unique<UnregisteredShape>();

    BOOST_TEST_CONTEXT("Accepted types: " << json::ShapeSerializers::getRegisteredTypes())
    {
        BOOST_CHECK_THROW(nlohmann::json j = shapeIn, error::NoSerializerForType);
    }
}


BOOST_AUTO_TEST_CASE(test_to_json_Unregistered_throws_no_exception_after_registration)
{
    json::ShapeSerializers::registerSerializer<UnregisteredShape>(::to_json, ::from_json);
    test_to_json_from_json_pointer<UnregisteredShape>();
}

BOOST_AUTO_TEST_CASE(test_to_json_Unregistered_throws_exception_when_already_registered)
{
    json::ShapeSerializers::registerSerializer<UnregisteredShape>(::to_json, ::from_json);
    BOOST_CHECK_THROW(json::ShapeSerializers::registerSerializer<UnregisteredShape>(::to_json, ::from_json),
                      error::SerializerAlreadyRegisteredForType);
}


namespace serial_overwrite
{
    static bool called = false;

    void to_json(nlohmann::json& j, const UnregisteredShape& rhs)
    {
        ::to_json(j, rhs);
        called = true;
    }
    void from_json(const nlohmann::json& j, UnregisteredShape& rhs)
    {
        ::from_json(j, rhs);
        called = true;
    }
}

BOOST_AUTO_TEST_CASE(test_to_json_Unregistered_allows_overwrite)
{
    json::ShapeSerializers::registerSerializer<UnregisteredShape>(::to_json, ::from_json);
    {
        serial_overwrite::called = false;
        nlohmann::json j = UnregisteredShape();
        BOOST_CHECK(!serial_overwrite::called);

        serial_overwrite::called = false;
        j.get<UnregisteredShape>();
        BOOST_CHECK(!serial_overwrite::called);
    }

    const bool overwrite = true;
    BOOST_CHECK_NO_THROW(json::ShapeSerializers::registerSerializer<UnregisteredShape>(
                             ::to_json, ::from_json, overwrite));
    {
        serial_overwrite::called = false;
        nlohmann::json j = UnregisteredShape();
        BOOST_CHECK(!serial_overwrite::called);

        serial_overwrite::called = false;
        j.get<UnregisteredShape>();
        BOOST_CHECK(!serial_overwrite::called);
    }
}


BOOST_AUTO_TEST_CASE(test_to_json_Registered)
{
    MemberRegisteredShape shapeIn = makeShape<MemberRegisteredShape>(42);

    BOOST_TEST_CONTEXT("Accepted types: " << json::ShapeSerializers::getRegisteredTypes())
    {
        nlohmann::json j = shapeIn;

        BOOST_TEST_CONTEXT(j.dump(2))
        {
            BOOST_CHECK(j.is_object());
        }
    }
}

BOOST_AUTO_TEST_CASE(test_to_json_from_json_ShapePtr_registered_via_member)
{
    test_to_json_from_json_pointer<MemberRegisteredShape>();
}


BOOST_AUTO_TEST_CASE(test_to_json_from_json_ShapePtr_registered_via_free_variable)
{
    test_to_json_from_json_pointer<FreeRegisteredShape>();
}


BOOST_AUTO_TEST_SUITE_END()
