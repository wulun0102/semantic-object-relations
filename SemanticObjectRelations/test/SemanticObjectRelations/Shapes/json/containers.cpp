
#include <SemanticObjectRelations/Shapes/json.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../../ShapeListFixture.h"


using namespace semrel;


struct ShapeListSerializationFixture
{
    ShapeList shapeList;

    ShapeListSerializationFixture()
    {
        // Ascending order of IDs so order is not changed through shape map.
        shapeList.emplace_back(test::makeShapePtr<Box>(45));
        shapeList.emplace_back(test::makeShapePtr<Cylinder>(57));
        shapeList.emplace_back(test::makeShapePtr<Sphere>(65));
        // shapeList.emplace_back(new MeshShape(ShapeID(3)));
    }
};


BOOST_FIXTURE_TEST_SUITE(Shape_Json_Containers, ShapeListSerializationFixture)


BOOST_AUTO_TEST_CASE(test_to_json_from_json_auto_ShapeList)
{
    nlohmann::json j;
    j = shapeList;

    BOOST_TEST_MESSAGE(j.dump(2));

    ShapeList shapeListOut;
    shapeListOut = j.get<ShapeList>();

    BOOST_REQUIRE_EQUAL(shapeListOut.size(), shapeList.size());
    for (std::size_t i = 0; i < shapeListOut.size(); ++i)
    {
        BOOST_CHECK(shapeListOut[i]);
        BOOST_CHECK_EQUAL(shapeListOut[i]->getID(), shapeList[i]->getID());
    }
}


BOOST_AUTO_TEST_CASE(test_to_json_from_json_auto_ShapeMap)
{
    ShapeMap shapeMap = toShapeMap(shapeList);

    nlohmann::json j;
    j = shapeMap;

    BOOST_TEST_MESSAGE(j.dump(2));

    ShapeList shapeListOut;
    shapeListOut = j.get<ShapeList>();

    BOOST_REQUIRE_EQUAL(shapeListOut.size(), shapeList.size());
    for (std::size_t i = 0; i < shapeListOut.size(); ++i)
    {
        BOOST_CHECK(shapeListOut[i]);
        BOOST_CHECK_EQUAL(shapeListOut[i]->getID(), shapeList[i]->getID());
    }
}


BOOST_AUTO_TEST_SUITE_END()
