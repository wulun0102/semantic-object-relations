#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Configuration of experiments and evaluation.

@author: kartmann
"""

#%%
import os

#%% Location and name of binary to run the experiments

experiment_binary_dir = "../SemanticObjectRelations/build/bin"
experiment_binary_name = "SemanticObjectRelationsExperiment"

experiment_binary = os.path.join(experiment_binary_dir, experiment_binary_name)


#%% Directories and param files

pointcloud_dir = "pointclouds"
ground_truth_dir = "ground_truths"
experiments_dir = "experiments"

params_common = "params-common.txt"
params_strategy = "params-strategy.txt"

output_graph_name = "graph"

evaluation_data_file = "evaluation_data.csv"

#%% names of all scenes

scene_names = []
scene_names += ["R" + str(i) for i in range(1, 9)] # real scenes
scene_names += ["S" + str(i) for i in range(1, 5)] # simulated scenes
scene_names += ["VL", "VP"] # validation scenes


#%% names of hypotheses

strategy_names = "a0 a10N a10S".split(" ")
strategy_names += [ n + "+UD" for n in strategy_names ]


#%%

relabel_suffix = "_relabeled"
