#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>

#include <SemantiX/interface/ShapeExtractor.ice>
#include <SemantiX/interface/SupportGraph.ice>

module armarx
{

    sequence<long> ShapeIdList;
    
    interface SupportAnalyzerInterface
    {
        SupportGraphBase extractSupportGraph(
                    ShapeBaseList objects, ShapeIdList safeObjectIDs);
    };

    interface SupportGraphListener
    {
        void reportSupportGraph(SupportGraphBase supportGraph);
    };
    
};
