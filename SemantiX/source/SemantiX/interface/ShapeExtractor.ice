#pragma once

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>

#include <SemantiX/interface/Shapes.ice>


module armarx
{

    interface ShapeExtractorInterface extends visionx::PointCloudProcessorInterface
    {
        /** 
         * Performs shape extraction from the latest received point cloud and 
         * returns the result.
         */
        ShapeBaseList extractShapes();
        
        /**
         * Returns the previously extracted shapes, if any.
         * (Call extractShapes() to trigger shape extraction).
         */
        ShapeBaseList getExtractedShapes();
    };

    interface ShapeExtractorListener
    {
        void reportExtractedShapes(ShapeBaseList shapes);
    };

};
    
