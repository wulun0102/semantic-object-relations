#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>


module armarx
{

    ["cpp:virtual"]
    class ShapeBase extends VariantDataClass
    {
        long id;
    };

    ["cpp:virtual"]
    class BoxBase extends ShapeBase
    {
        Vector3Base position;
        QuaternionBase orientation;
        Vector3Base extents;
    };

    ["cpp:virtual"]
    class CylinderBase extends ShapeBase
    {
        Vector3Base position;
        Vector3Base direction;
        float radius;
        float height;
    };

    ["cpp:virtual"]
    class SphereBase extends ShapeBase
    {
        Vector3Base position;
        float radius;
    };

    ["cpp:virtual"]
    class MeshShapeBase extends ShapeBase
    {
        Vector3Base position;
        QuaternionBase orientation;
        DebugDrawerTriMesh mesh;
    };

    sequence<ShapeBase> ShapeSequence;

    class ShapeBaseList extends VariantDataClass
    {
        ShapeSequence list;
    };

};

module semantix
{
    struct TimedShapeBaseList
    {
        long timestampMicroseconds;
        armarx::ShapeBaseList shapes;
    };
};
