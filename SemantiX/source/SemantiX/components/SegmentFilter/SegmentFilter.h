/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SegmentFilter
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

//#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <SemantiX/interface/SegmentFilter.h>


using LabeledPointCloud = pcl::PointCloud<pcl::PointXYZRGBL>;
using LabeledPointCloudPtr = LabeledPointCloud::Ptr;

namespace armarx
{
    /// @class SegmentFilterPropertyDefinitions
    class SegmentFilterPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        SegmentFilterPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-SegmentFilter SegmentFilter
     * @ingroup SemantiX-Components
     * A description of the component SegmentFilter.
     *
     * @class SegmentFilter
     * @ingroup Component-SegmentFilter
     * @brief Brief description of class SegmentFilter.
     *
     * Detailed description of class SegmentFilter.
     */
    class SegmentFilter :
        virtual public visionx::PointCloudProcessor,
        virtual public armarx::SegmentFilterInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // SegmentFilterInterface interface
        void setNumSegments(Ice::Int numSegments, const Ice::Current&) override;


    protected:

        // PointCloudProcessor interface
        void onInitPointCloudProcessor() override;
        void onConnectPointCloudProcessor() override;
        void onExitPointCloudProcessor() override;
        void process() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        LabeledPointCloudPtr filterSegments(LabeledPointCloudPtr pointCloud);
        LabeledPointCloudPtr pickSegment(LabeledPointCloudPtr pointCloud);

        /**
         * @brief Performs an in-place transformation of the given point cloud
         * from source frame to target frame.
         * @param pointCloud the point cloud
         */
        void transformPointCloud(LabeledPointCloud& pointCloud);

        void publishOutputPointCloud(const LabeledPointCloudPtr& pointCloud);

        std::vector<std::vector<int>> getSegmentIndices(const LabeledPointCloud& pc);


    private:

        bool enabled = true;

        /// Name of result point cloud.
        std::string resultPointCloudName;

        /// Name of the offered topic.
        std::string offeredTopicName;

        /// The proxy listening for trimmed point clouds.
        armarx::SegmentFilterListenerPrx segmentFilterListenerPrx;


        /// The robot state component.
        RobotStateComponentInterfacePrx robotStateComponent;
        /// The local robot.
        VirtualRobot::RobotPtr robot;


        std::string sourceFrame;
        std::string targetFrame;

        /// If true, do not discard points.
        bool enableFiltering = true;

        std::size_t keepBiggestSegmentsNum = 0;
        std::size_t segmentSizeThreshold = 0;

        /// If >= 0, pick the specific segment. Keep all otherwise.
        int pickSegmentLabel = -1;

    };
}
