/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SegmentFilter
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SegmentFilter.h"

#include <queue>

#include <pcl/common/transforms.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <VisionX/components/pointcloud_core/PointCloudConversions.h>


using namespace armarx;


SegmentFilterPropertyDefinitions::SegmentFilterPropertyDefinitions(std::string prefix) :
    visionx::PointCloudProcessorPropertyDefinitions(prefix)
{
    // ENABLING
    defineOptionalProperty<bool>(
        "Enable", true,
        "If false, does not register as point cloud processor and does not provide any point clouds.");


    // TOPICS

    defineOptionalProperty<std::string>(
        "ResultTopicName", "SegmentFilterUpdates",
        "Name of topic where filtered segments (point cloud) are published.\n"
        "Leave empty to disable.");


    // POINT CLOUDS

    defineOptionalProperty<std::string>(
        "ResultCloudName", "SegmentFilterResult",
        "Name of the output point cloud with filtered segments cloud.");


    // OPTIONS


    defineOptionalProperty<std::string>(
        "TargetFrameName", "Global",
        "Frame to which the point cloud shall be transformed.\n"
        "Leave empty to disable.");

    defineOptionalProperty<std::string>(
        "RobotStateComponentName", "RobotStateComponent",
        "Name of the robot state component.");


    defineOptionalProperty<bool>(
        "EnableFiltering", true,
        "If false, no points will be discarded.");

    defineOptionalProperty<long>(
        "KeepBiggestSegmentsNum", 0,
        "If > 0, keep only this number of segments with the most points.")
    .setMin(0);

    defineOptionalProperty<long>(
        "SegmentSizeThreshold", 0,
        "If > 0, discard all segments with less than this amount of points.")
    .setMin(0);

    defineOptionalProperty<int>(
        "PickSegmentLabel", -1,
        "If >= 0, pick the segment with the given ID.\n"
        "To disable, set to -1. (Used for testing.)");

}


std::string SegmentFilter::getDefaultName() const
{
    return "SegmentFilter";
}


void SegmentFilter::onInitPointCloudProcessor()
{
    getProperty(enabled, "Enable");
    if (!enabled)
    {
        return;
    }

    // Input + Output
    getProperty(resultPointCloudName, "ResultCloudName");
    getProperty(offeredTopicName, "ResultTopicName");

    getProperty(targetFrame, "TargetFrameName");

    getProperty(enableFiltering, "EnableFiltering");
    keepBiggestSegmentsNum = static_cast<std::size_t>(getProperty<long>("KeepBiggestSegmentsNum"));
    segmentSizeThreshold = static_cast<std::size_t>(getProperty<long>("SegmentSizeThreshold"));
    getProperty(pickSegmentLabel, "PickSegmentLabel");

    // Registering

    usingProxyFromProperty("RobotStateComponentName");
    if (offeredTopicName.empty())
    {
        offeringTopic(offeredTopicName);
    }
}

void SegmentFilter::onConnectPointCloudProcessor()
{
    if (!enabled)
    {
        return;
    }

    enableResultPointClouds<pcl::PointXYZRGBL>(resultPointCloudName);

    getProxyFromProperty(robotStateComponent, "RobotStateComponentName");
    robot = RemoteRobot::createLocalClone(robotStateComponent);

    getTopic(segmentFilterListenerPrx, offeredTopicName);

    sourceFrame = PointCloudProcessor::getPointCloudFrame(getPointCloudProviderNames().front());
    ARMARX_INFO << "Point cloud source frame: " << sourceFrame;
}

void SegmentFilter::onExitPointCloudProcessor()
{

}

void SegmentFilter::process()
{
    if (!enabled)
    {
        return;
    }

    if (!waitForPointClouds(10000))
    {
        ARMARX_WARNING << "Timeout or error in wait for pointclouds";
        return;
    }

    // Retrieve pointclouds

    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (!getPointClouds<pcl::PointXYZRGBL>(pointCloudPtr))
    {
        ARMARX_WARNING << "Unable to get point cloud data.";
        return;
    }

    ARMARX_VERBOSE << deactivateSpam(5)
                   << "Received point cloud with " << pointCloudPtr->size() << " points.";


    // PROCESS

    // Filter
    LabeledPointCloudPtr outputPointCloud = filterSegments(pointCloudPtr);
    outputPointCloud = pickSegment(outputPointCloud);

    // Transform
    transformPointCloud(*outputPointCloud);

    // PROCESS END


    ARMARX_INFO << deactivateSpam(5)
                << "Publishing point cloud of filtered segments ... ("
                << outputPointCloud->size() << " points)";

    provideResultPointClouds<pcl::PointXYZRGBL>(outputPointCloud, resultPointCloudName);
    publishOutputPointCloud(outputPointCloud);
}

void SegmentFilter::publishOutputPointCloud(const LabeledPointCloudPtr& pointCloud)
{
    if (offeredTopicName.empty())
    {
        return;
    }

    visionx::ColoredLabeledPointCloud cloud;
    cloud.reserve(pointCloud->size());

    auto pclToVisionX = [](const pcl::PointXYZRGBL & p)
    {
        return visionx::ColoredLabeledPoint3D
        {
            visionx::Point3D { p.x, p.y, p.z },
            visionx::RGBA { p.r, p.g, p.b, p.a },
            static_cast<int>(p.label)
        };
    };

    std::transform(pointCloud->begin(), pointCloud->end(), std::back_inserter(cloud), pclToVisionX);

    segmentFilterListenerPrx->reportFilteredSegments(cloud);
}


LabeledPointCloudPtr SegmentFilter::filterSegments(LabeledPointCloudPtr pointCloud)
{
    LabeledPointCloudPtr filtered(new LabeledPointCloud());

    if (!enableFiltering)
    {
        ARMARX_INFO << "Not applying filtering (disabled).";
        return pointCloud;
    }

    // filter

    std::vector<std::vector<int>> segmentIndices = getSegmentIndices(*pointCloud);
    bool appliedFilter = false;

    if (segmentSizeThreshold > 0)
    {
        ARMARX_INFO << "Filtering segments per size (threshold: " << segmentSizeThreshold << ")";
        std::vector<std::size_t> segmentsToKeep;
        std::stringstream ss;
        ss << "Segment sizes: \n";

        for (std::size_t seg = 0; seg < segmentIndices.size(); ++seg)
        {
            ss << seg << ": " << segmentIndices[seg].size();
            if (segmentIndices[seg].size() >= segmentSizeThreshold)
            {
                ss << " => KEPT";
                segmentsToKeep.push_back(seg);
            }
            else
            {
                ss << " => discarded";
            }
            ss << "\n";
        }

        ARMARX_VERBOSE << ss.str();

        std::vector<int> indicesToKeep;
        std::vector<std::vector<int>> newSegmentIndices;

        for (std::size_t seg : segmentsToKeep)
        {
            indicesToKeep.insert(indicesToKeep.end(), segmentIndices[seg].begin(),
                                 segmentIndices[seg].end());
            newSegmentIndices.push_back(segmentIndices[seg]);
        }

        // replace filtered by reduced pc
        LabeledPointCloudPtr keptPC(new LabeledPointCloud(*pointCloud, indicesToKeep));
        filtered = keptPC;
        appliedFilter = true;
        segmentIndices = newSegmentIndices;
    }

    if (keepBiggestSegmentsNum > 0 && keepBiggestSegmentsNum < segmentIndices.size())
    {
        ARMARX_INFO << "Filtering to keep the " << keepBiggestSegmentsNum << " biggest segments.";

        // find biggest segments using priority queue

        // we need a min-queue (to compare to the min element and keep greater elements)
        // default is max-qeue with less comparator, so we use greater
        using SizeIndex = std::pair<std::size_t, std::size_t>; // (size[seg], seg)
        using Container = std::vector<SizeIndex>;
        using Compare = std::greater<SizeIndex>;

        std::priority_queue<SizeIndex, Container, Compare> queue;

        auto addToQueue = [&](std::size_t seg)
        {
            queue.push(std::make_pair(segmentIndices[seg].size(), seg));
        };

        // populate queue with first k elements
        for (std::size_t seg = 0; seg < keepBiggestSegmentsNum; seg++)
        {
            addToQueue(seg);
        }

        // check the rest
        for (std::size_t seg = keepBiggestSegmentsNum; seg < segmentIndices.size(); seg++)
        {
            // check whether seg is bigger than smallest element
            // queue.top() is the smallest current segment
            if (queue.top().first < segmentIndices[seg].size())
            {
                queue.pop(); // pop min element
                addToQueue(seg);
            }
        }

        // queue now containts indices of biggest segments
        std::vector<int> indicesToKeep;
        std::vector<std::vector<int>> newSegmentIndices;

        while (!queue.empty())
        {
            std::size_t seg = queue.top().second;
            indicesToKeep.insert(indicesToKeep.end(), segmentIndices[seg].begin(),
                                 segmentIndices[seg].end());
            newSegmentIndices.push_back(segmentIndices[seg]);
            queue.pop();
        }

        // replace filtered by reduced pc
        LabeledPointCloudPtr keptPC(new LabeledPointCloud(*pointCloud, indicesToKeep));
        filtered = keptPC;
        appliedFilter = true;
        segmentIndices = newSegmentIndices;
    }

    if (!appliedFilter)
    {
        filtered = pointCloud;
    }

    return filtered;
}

LabeledPointCloudPtr SegmentFilter::pickSegment(LabeledPointCloudPtr pointCloud)
{
    if (pickSegmentLabel >= 0)
    {
        ARMARX_INFO << "Picking segment " << pickSegmentLabel;
        // find indices
        std::vector<int> indices;
        for (std::size_t i = 0; i < pointCloud->size(); ++i)
        {
            pcl::PointXYZRGBL p = pointCloud->at(i);
            if (static_cast<long>(p.label) == pickSegmentLabel)
            {
                indices.push_back(static_cast<int>(i));
            }
        }

        LabeledPointCloudPtr segment(new LabeledPointCloud(*pointCloud, indices));
        return segment;
    }
    else
    {
        return pointCloud;
    }
}

void SegmentFilter::transformPointCloud(LabeledPointCloud& pointCloud)
{
    if (targetFrame == "" || sourceFrame == "" || targetFrame == sourceFrame)
    {
        return;
    }

    ARMARX_INFO << "Transforming point cloud from frame " << sourceFrame
                << " to frame " << targetFrame;

    RemoteRobot::synchronizeLocalClone(robot, robotStateComponent);

    Eigen::Matrix4f sourceToRoot = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f rootToTarget = Eigen::Matrix4f::Identity();

    if (sourceFrame == GlobalFrame)
    {
        sourceToRoot = robot->getRootNode()->getGlobalPose().inverse();
    }
    else if (robot->hasRobotNode(sourceFrame))
    {
        sourceToRoot = robot->getRobotNode(sourceFrame)->getPoseInRootFrame();
    }
    else
    {
        ARMARX_ERROR << "Invalid source frame: Robot has no node " << sourceFrame;
        return;
    }

    if (targetFrame == GlobalFrame)
    {
        rootToTarget = robot->getRootNode()->getGlobalPose();
    }
    else if (robot->hasRobotNode(targetFrame))
    {
        rootToTarget = robot->getRobotNode(targetFrame)->getPoseInRootFrame().inverse();
    }
    else
    {
        ARMARX_ERROR << "Invalid target frame: Robot has no node " << targetFrame;
        return;
    }

    Eigen::Matrix4f sourceToTarget = rootToTarget * sourceToRoot;

    ARMARX_VERBOSE << "Transformation matrix (source -> root -> target): \n" << sourceToTarget;

    pcl::transformPointCloud(pointCloud, pointCloud, sourceToTarget);
}


std::vector<std::vector<int>> SegmentFilter::getSegmentIndices(const LabeledPointCloud& pc)
{
    std::vector<std::vector<int>> segmentIndices;

    for (std::size_t i = 0; i < pc.size(); ++i)
    {
        const pcl::PointXYZRGBL& p = pc[i];
        std::size_t segment = p.label;

        if (segment >= segmentIndices.size())
        {
            segmentIndices.resize(segment + 1);
        }

        segmentIndices[segment].push_back(static_cast<int>(i));
    }

    return segmentIndices;
}

void SegmentFilter::setNumSegments(Ice::Int numSegments, const Ice::Current&)
{
    keepBiggestSegmentsNum = numSegments >= 0 ? static_cast<std::size_t>(numSegments) : 0;
}



armarx::PropertyDefinitionsPtr SegmentFilter::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new SegmentFilterPropertyDefinitions(
            getConfigIdentifier()));
}

