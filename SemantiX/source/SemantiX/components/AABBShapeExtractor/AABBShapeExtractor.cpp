/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::AABBShapeExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AABBShapeExtractor.h"

#include <ArmarXCore/observers/variant/Variant.h>

#include <VisionX/components/pointcloud_core/PCLUtilities.h>

#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>

#include "shapes_from_aabbs.h"


namespace semantix
{

    AABBShapeExtractorPropertyDefinitions::AABBShapeExtractorPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("ice.ShapeTopicName", "ShapeUpdates",
                                            "Topic where shapes are published.");
        defineOptionalProperty<std::string>("ice.DebugObserverName", "DebugObserver",
                                            "Name of the debug observer.");

        defineOptionalProperty<float>("OutlierRate", 0.01f, "Allowed outliers for AABBs.");
    }

    std::string AABBShapeExtractor::getDefaultName() const
    {
        return "AABBShapeExtractor";
    }


    void AABBShapeExtractor::onInitPointCloudProcessor()
    {
        offeringTopicFromProperty("ice.ShapeTopicName");
        offeringTopicFromProperty("ice.DebugObserverName");
        debugDrawer.offeringTopic(*this);
    }


    void AABBShapeExtractor::onConnectPointCloudProcessor()
    {
        getTopicFromProperty(shapeListener, "ice.ShapeTopicName");
        getTopicFromProperty(debugObserver, "ice.DebugObserverName");
        debugDrawer.getTopic(*this);
    }


    void AABBShapeExtractor::onDisconnectPointCloudProcessor()
    {
    }

    void AABBShapeExtractor::onExitPointCloudProcessor()
    {
    }


    void AABBShapeExtractor::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());
        if (waitForPointClouds())
        {
            getPointClouds<PointT>(inputCloud);
        }
        else
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data";
            return;
        }

        // Do processing.

        float outlierRate = 0;
        getProperty(outlierRate, "OutlierRate");

        semrel::ShapeList shapes;
        if (outlierRate > 0 && outlierRate < 1)
        {
            shapes = semrel::getShapesFromSoftAABBs(*inputCloud, outlierRate);
        }
        else
        {
            shapes = semrel::getShapesFromAABBs(*inputCloud);
        }

        debugObserver->setDebugChannel(
            getName(),
        {
            { "Point Cloud Size", new armarx::Variant(static_cast<int>(inputCloud->size())) },
            { "Point Cloud Time", new armarx::Variant(static_cast<int>(inputCloud->header.stamp)) },
            { "Num AABBs", new armarx::Variant(static_cast<int>(shapes.size())) },
        });

        // Publish shapes.
        const uint64_t timestamp = inputCloud->header.stamp;
        shapeListener->reportShapes({static_cast<Ice::Long>(timestamp), new armarx::ShapeVariantList(shapes)});
    }


    armarx::PropertyDefinitionsPtr AABBShapeExtractor::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new AABBShapeExtractorPropertyDefinitions(
                getConfigIdentifier()));
    }

}
