/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SupportAnalyzer
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SupportAnalyzer.h"

#include <RobotAPI/libraries/core/FramedPose.h>

#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>
#include <SemantiX/libraries/SemantiXVariants/SupportGraphVariant.h>

#include <SemantiX/libraries/SemanticObjectRelationsHooks/ArmarXLog.h>
#include <SemantiX/libraries/SemanticObjectRelationsHooks/ArmarXVisualizer.h>

#include <SemanticObjectRelations/SupportAnalysis/json.h>
#include <SimoxUtility/color.h>


using namespace armarx;

SupportAnalyzerPropertyDefinitions::SupportAnalyzerPropertyDefinitions(std::string prefix) :
    armarx::ComponentPropertyDefinitions(prefix)
{
    // PARAMETERS

    defineOptionalProperty<float>(
        "GR.ContactMargin", 10.,
        "Distance by which objects are increased for contact computation [mm].")
    .setMin(0.);

    defineOptionalProperty<float>(
        "GR.VerticalSepPlaneAngleMax", 10.,
        "Maximal angle [degree] between gravity and separating plane for \n"
        "separating plane to be considered vertical.")
    .setMin(0.);

    defineOptionalProperty<bool>(
        "GR.VerticalSepPlaneAssumeSupport", false,
        "If true, edges are added if the separating plane is vertical.");


    defineOptionalProperty<bool>(
        "UD.enabled", true,
        "Enable or disble uncertainty detection (UD).");

    defineOptionalProperty<float>(
        "UD.SupportAreaRatioMin", 0.7f,
        "Minimal support area ratio of an object to consider it safe.")
    .setMin(0.).setMax(1.);


    // TOPICS & PROXIES

    // USING

    defineOptionalProperty<std::string>(
        "RobotStateComponentName", "RobotStateComponent",
        "Name of the robot state component.");

    defineOptionalProperty<std::string>(
        "PointCloudFrameName", "Global",
        "Name of the frame of the point cloud coordinates. \nUsed to determine the gravity vector.");


    // OFFERING

    defineOptionalProperty<std::string>(
        "SupportGraphTopicName", "SupportGraphUpdates",
        "Name of the topic where the support graph shall be published.");


    // VISUALIZATION

    defineOptionalProperty<bool>(
        "VerboseVisualization", false,
        "True for verbose visualization, false for result-only visualization.");
}


std::string SupportAnalyzer::getDefaultName() const
{
    return "SupportAnalyzer";
}


void SupportAnalyzer::onInitComponent()
{
    offeringTopicFromProperty("SupportGraphTopicName");
    debugDrawer.offeringTopic(*this);

    usingProxyFromProperty("RobotStateComponentName");

    getProperty(pointCloudFrameName, "PointCloudFrameName");

    semrel::LogInterface::setImplementation(std::make_shared<ArmarXLog>(getName()));
    // set log level to debug, ArmarXLog will filter to component's log level
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::DEBUG);

    supportAnalysis.setContactMargin(getProperty<float>("GR.ContactMargin"));
    supportAnalysis.setVertSepPlaneAngleMax(getProperty<float>("GR.VerticalSepPlaneAngleMax"));
    supportAnalysis.setVertSepPlaneAssumeSupport(getProperty<bool>("GR.VerticalSepPlaneAssumeSupport"));

    supportAnalysis.setUncertaintyDetectionEnabled(getProperty<bool>("UD.enabled"));
    supportAnalysis.setSupportAreaRatioMin(getProperty<float>("UD.SupportAreaRatioMin"));

    getProperty(verboseVisualization, "VerboseVisualization");
}


void SupportAnalyzer::onConnectComponent()
{
    getProxyFromProperty(robotStateComponent, "RobotStateComponentName");
    getTopicFromProperty(supportGraphListener, "SupportGraphTopicName");
    debugDrawer.getTopic(*this);

    // set visualizer hook
    semrel::VisualizerInterface::setImplementation(std::make_shared<ArmarXVisualizer>(debugDrawer));

    semrel::VisuLevel visuLevel = verboseVisualization
                                  ? semrel::VisuLevel::VERBOSE
                                  : semrel::VisuLevel::RESULT;

    semrel::VisualizerInterface::setMinimumVisuLevel(visuLevel);
}


void SupportAnalyzer::onDisconnectComponent()
{

}


void SupportAnalyzer::onExitComponent()
{

}


SupportGraphBasePtr SupportAnalyzer::extractSupportGraph(
    const ShapeBaseListPtr& objectsBaseList,
    const ShapeIdList& safeObjectIdList,
    const Ice::Current&)
{
    semrel::ShapeList objects = ShapeVariantListPtr::dynamicCast(objectsBaseList)->toShapeList();

    std::set<semrel::ShapeID> safeObjectIDs;
    for (long id : safeObjectIdList)
    {
        safeObjectIDs.insert(semrel::ShapeID { id });
    }

    // update gravity vector
    supportAnalysis.setGravityVector(getGravityFromRobotStateComponent());

    semrel::SupportGraph supportGraph = supportAnalysis.performSupportAnalysis(objects, safeObjectIDs);
    ARMARX_INFO << "Support Graph: " << supportGraph.str();

    semrel::AttributedGraph attributedGraph = semrel::toAttributedGraph(supportGraph);
    for (auto vertex : attributedGraph.vertices())
    {
        int segmentID = vertex.attrib().objectID.t;
        Eigen::Vector4i segmentColor = simox::color::GlasbeyLUT::at(segmentID).to_vector4i();
        Eigen::Vector4i black(0, 0, 0, 255);
        Eigen::Vector4i white(255, 255, 255, 255);

        int sumSegColor = segmentColor(0) + segmentColor(1) + segmentColor(2);
        float averageSegColor = sumSegColor / 3.0f;
        Eigen::Vector4i fontColor;
        if (averageSegColor > 127.0f)
        {
            fontColor = black;
        }
        else
        {
            fontColor = white;
        }

        auto& style = vertex.attrib().json["style"];
        style["fill-color"] = segmentColor;
        style["border-color"] = black;
        style["font-color"] = fontColor;
    }

    for (auto edge : supportGraph.edges())
    {
        bool vertical = edge.attrib().verticalSeparatingPlane;
        bool topDown = edge.attrib().fromUncertaintyDetection;

         Eigen::Vector4i color(0, 0, 0, 255);
        if (vertical)
        {
            // Blue
            color = Eigen::Vector4i(0, 0, 255, 255);
        }
        if (topDown)
        {
            // Red
            color = Eigen::Vector4i(255, 0, 0, 255);
        }

        auto aEdge = attributedGraph.edge(attributedGraph.vertex(edge.sourceObjectID()), attributedGraph.vertex(edge.targetObjectID()));
        auto& style = aEdge.attrib().json["style"];
        style["color"] = color;
    }
    storeGraph("SupportGraph", attributedGraph);

    SupportGraphVariantPtr supportGraphVariant = new SupportGraphVariant(supportGraph);

    return supportGraphVariant;
}


Eigen::Vector3f SupportAnalyzer::getGravityFromRobotStateComponent()
{
    const Eigen::Vector3f gravityInGlobal = - Eigen::Vector3f::UnitZ();

    if (pointCloudFrameName == armarx::GlobalFrame)
    {
        return gravityInGlobal;
    }
    else
    {
        VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(robotStateComponent);

        // Define gravity in global
        FramedDirection dir(gravityInGlobal, armarx::GlobalFrame, "");
        dir.changeFrame(robot, pointCloudFrameName);
        return dir.toEigen();
    }
}



armarx::PropertyDefinitionsPtr SupportAnalyzer::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new SupportAnalyzerPropertyDefinitions(
            getConfigIdentifier()));
}


