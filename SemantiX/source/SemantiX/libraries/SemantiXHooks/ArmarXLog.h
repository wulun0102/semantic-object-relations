#pragma once

#include <SemanticObjectRelations/Hooks/Log.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx
{

    class ArmarXLog : public semrel::LogInterface, public Logging
    {

    public:

        static void setAsImplementation();


        ArmarXLog(const std::string& tag = "");


        virtual void log(semrel::LogMetaInfo info, const std::string& message) override;


    private:

        LogSenderPtr getLogSender(const semrel::LogMetaInfo& info);

        armarx::MessageTypeT logLevelToMessageType(semrel::LogLevel level);

    };

}
