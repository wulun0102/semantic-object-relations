set(LIB_NAME       SemantiXVariants)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")


set(LIBS
    RobotAPICore
    ${SemanticObjectRelations_LIBRARIES}
    SemantiXInterfaces
)

set(LIB_FILES
    SemantiXVariantsFactories.cpp
    ShapeVariants.cpp
    SupportGraphVariant.cpp
)
set(LIB_HEADERS
    SemantiXVariantsFactories.h
    ShapeVariants.h
    SupportGraphVariant.h
    VariantListSerialization.h
)


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

# add unit tests
add_subdirectory(test)
