#include "SupportGraphVariant.h"

#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include "VariantListSerialization.h"


using namespace armarx;

SupportVertexVariant::SupportVertexVariant()
{
}

SupportVertexVariant::SupportVertexVariant(long shapeID, bool udEnabled, float udSupportAreaRatio, bool udSafe) :
    SupportVertexBase(shapeID, udEnabled, udSupportAreaRatio, udSafe)
{
}

SupportVertexVariant::SupportVertexVariant(const semrel::SupportGraph::ConstVertex& vertex) :
    SupportVertexVariant(
        static_cast<long>(vertex.objectID()),
        vertex.attrib().ud.enabled,
        vertex.attrib().ud.supportAreaRatio,
        vertex.attrib().ud.safe)
{
}

semrel::SupportVertex SupportVertexVariant::toVertexAttrib() const
{
    semrel::SupportVertex sv;

    sv.objectID = semrel::ShapeID(shapeID);
    sv.ud.enabled = udEnabled;
    sv.ud.supportAreaRatio = udSupportAreaRatio;
    sv.ud.safe = udSafe;

    return sv;
}

Ice::ObjectPtr SupportVertexVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr SupportVertexVariant::clone(const Ice::Current&) const
{
    return new SupportVertexVariant(*this);
}

std::string SupportVertexVariant::output(const Ice::Current&) const
{
    const std::string falseTrue[] = { "false", "true" };
    std::stringstream ss;
    ss << "SupportVertexVariant [ "
       << "ShapeID: " << shapeID
       << "| UD: enabled: " << falseTrue[udEnabled] << ", "
       << "supp. area ratio: " << udSupportAreaRatio << ", "
       << "safe: " << falseTrue[udSafe]
       << " ]";
    return ss.str();
}

Ice::Int SupportVertexVariant::getType(const Ice::Current&) const
{
    return VariantType::SupportVertexVariant;
}

bool SupportVertexVariant::validate(const Ice::Current&)
{
    return true;
}

void SupportVertexVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("shapeID", static_cast<int>(shapeID));
    obj->setBool("ud_enabled", udEnabled);
    obj->setFloat("ud_supportAreaRatio", udSupportAreaRatio);
    obj->setBool("ud_safe", udSafe);
}

void SupportVertexVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    shapeID = static_cast<long>(obj->getInt("shapeID"));
    udEnabled = obj->getBool("ud_enabled");
    udSupportAreaRatio = obj->getFloat("ud_supportAreaRatio");
    udSafe = obj->getBool("ud_safe");
}

SupportEdgeVariant::SupportEdgeVariant()
{
}

SupportEdgeVariant::SupportEdgeVariant(
    long sourceID, long targetID, bool verticalSeparatingPlane, bool fromUncertaintyDetection) :
    SupportEdgeBase(sourceID, targetID, verticalSeparatingPlane, fromUncertaintyDetection)
{
}

SupportEdgeVariant::SupportEdgeVariant(const semrel::SupportGraph::ConstEdge& edge) :
    SupportEdgeVariant(static_cast<long>(edge.sourceObjectID()),
                       static_cast<long>(edge.targetObjectID()),
                       edge.attrib().verticalSeparatingPlane,
                       edge.attrib().fromUncertaintyDetection)
{
}

semrel::SupportEdge SupportEdgeVariant::toEdgeAttrib() const
{
    semrel::SupportEdge se;
    se.verticalSeparatingPlane = verticalSeparatingPlane;
    se.fromUncertaintyDetection = fromUncertaintyDetection;
    return se;
}

Ice::ObjectPtr SupportEdgeVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr SupportEdgeVariant::clone(const Ice::Current&) const
{
    return new SupportEdgeVariant(*this);
}

std::string SupportEdgeVariant::output(const Ice::Current&) const
{
    const std::string falseTrue[] = { "false", "true" };
    std::stringstream ss;
    ss << "SupportEdgeVariant [ "
       << sourceID << " -> " << targetID << " | "
       << "from UD: " << falseTrue[fromUncertaintyDetection] << ", "
       << "vert. sep. plane: " << falseTrue[verticalSeparatingPlane]
       << " ]";
    return ss.str();
}

Ice::Int SupportEdgeVariant::getType(const Ice::Current&) const
{
    return VariantType::SupportEdgeVariant;
}

bool SupportEdgeVariant::validate(const Ice::Current&)
{
    return true;
}

void SupportEdgeVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("sourceID", int(sourceID));
    obj->setInt("targetID", int(targetID));
    obj->setBool("verticalSeparatingPlane", verticalSeparatingPlane);
    obj->setBool("fromUncertaintyDetection", fromUncertaintyDetection);
}

void SupportEdgeVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    sourceID = long(obj->getInt("sourceID"));
    targetID = long(obj->getInt("targetID"));
    verticalSeparatingPlane = obj->getBool("verticalSeparatingPlane");
    fromUncertaintyDetection = obj->getBool("fromUncertaintyDetection");
}



SupportGraphVariant::SupportGraphVariant() : SupportGraphBase()
{

}


SupportGraphVariant::SupportGraphVariant(
    const SupportVertexBaseList& vertices, const SupportEdgeBaseList& edges) :
    SupportGraphBase(vertices, edges)
{
}


SupportGraphVariant::SupportGraphVariant(const semrel::SupportGraph& supportGraph)
{
    for (auto v : supportGraph.vertices())
    {
        vertices.push_back(new SupportVertexVariant(v));
    }

    for (auto e : supportGraph.edges())
    {
        edges.push_back(new SupportEdgeVariant(e));
    }
}

semrel::SupportGraph SupportGraphVariant::toSupportGraph() const
{
    semrel::SupportGraph graph;

    for (auto v : vertices)
    {
        addVertexFromVariant(graph, v);
    }

    for (auto e : edges)
    {
        addEdgeFromVariant(graph, e);
    }

    return graph;
}


Ice::ObjectPtr SupportGraphVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr SupportGraphVariant::clone(const Ice::Current&) const
{
    return new SupportGraphVariant(*this);
}

std::string SupportGraphVariant::output(const Ice::Current&) const
{
    std::stringstream ss;
    ss << "+ SupportGraphVariant\n"
       << "+== Vertices ==\n";

    for (auto v : vertices)
    {
        ss << "| " << v->output() << "\n";
    }
    ss << "+== Edges ==\n";
    for (auto e : edges)
    {
        ss << "| " << e->output() << "\n";
    }

    return ss.str();
}

Ice::Int SupportGraphVariant::getType(const Ice::Current&) const
{
    return VariantType::SupportGraphVariant;
}

bool SupportGraphVariant::validate(const Ice::Current&)
{
    return true;
}


void SupportGraphVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    serializeVariantList<SupportVertexBasePtr>(vertices, "vertices", serializer, VariantPtrSerializer<SupportVertexBasePtr>);
    serializeVariantList<SupportEdgeBasePtr>(edges, "edges", serializer, VariantPtrSerializer<SupportEdgeBasePtr>);
}

void SupportGraphVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    vertices.clear();
    edges.clear();
    deserializeVariantList<SupportVertexBasePtr>(vertices, "vertices", serializer, VariantPtrDeserializer<SupportVertexVariant>);
    deserializeVariantList<SupportEdgeBasePtr>(edges, "edges", serializer, VariantPtrDeserializer<SupportEdgeVariant>);
}


void SupportGraphVariant::addVertexFromVariant(
    semrel::SupportGraph& graph, const SupportVertexBasePtr& vertex, const semrel::Shape* object)
{
    SupportVertexVariantPtr var = SupportVertexVariantPtr::dynamicCast(vertex);
    if (!var)
    {
        throw std::invalid_argument("Vertex is not a SupportVertexVariant.");
    }
    auto v = graph.addVertex(semrel::ShapeID(var->shapeID), var->toVertexAttrib());
    if (object)
    {
        v.attrib().objectID = object->getID();
    }
}

void SupportGraphVariant::addEdgeFromVariant(semrel::SupportGraph& graph, const SupportEdgeBasePtr& edge)
{
    if (!SupportEdgeVariantPtr::dynamicCast(edge))
    {
        throw std::invalid_argument("Edge is not a SupportVertexEdge.");
    }
    SupportEdgeVariantPtr var = SupportEdgeVariantPtr::dynamicCast(edge);
    graph.addEdge(semrel::ShapeID(var->sourceID), semrel::ShapeID(var->targetID),
                  var->toEdgeAttrib());
}



SupportGraphVariantPtr armarx::frost(const semrel::SupportGraph& supportGraph)
{
    return new SupportGraphVariant(supportGraph);
}

semrel::SupportGraph armarx::defrost(const SupportGraphBasePtr& supportGraph)
{
    return SupportGraphVariantPtr::dynamicCast(supportGraph)->toSupportGraph();
}
