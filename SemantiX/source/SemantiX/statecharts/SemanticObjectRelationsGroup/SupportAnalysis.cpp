/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::SemanticObjectRelationsGroup
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SupportAnalysis.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace SemanticObjectRelationsGroup;

// DO NOT EDIT NEXT LINE
SupportAnalysis::SubClassRegistry SupportAnalysis::Registry(SupportAnalysis::GetName(), &SupportAnalysis::CreateInstance);



void SupportAnalysis::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void SupportAnalysis::run()
{
    ShapeVariantListPtr objects = in.getObjects();

    ShapeIdList safeObjectIDs;

    for (int id : in.getSafeObjectIDs())
    {
        safeObjectIDs.push_back(ShapeIdList::value_type(id));
    }


    SupportGraphBasePtr supportGraph = getSupportAnalyzer()->extractSupportGraph(
                                           objects, safeObjectIDs);

    out.setSupportGraph(supportGraph);

    emitSuccess();
}

//void SupportAnalysis::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SupportAnalysis::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SupportAnalysis::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SupportAnalysis(stateData));
}

