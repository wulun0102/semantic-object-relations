/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::SemanticObjectRelationsGroup
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShapeExtraction.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace SemanticObjectRelationsGroup;

// DO NOT EDIT NEXT LINE
ShapeExtraction::SubClassRegistry ShapeExtraction::Registry(ShapeExtraction::GetName(), &ShapeExtraction::CreateInstance);



void ShapeExtraction::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ShapeExtraction::run()
{
    ShapeBaseListPtr extractedObjects = getShapeExtractor()->extractShapes();
    out.setObjects(extractedObjects);

    emitSuccess();
}

//void ShapeExtraction::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ShapeExtraction::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ShapeExtraction::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ShapeExtraction(stateData));
}

